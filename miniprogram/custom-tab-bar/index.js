Component({
  data: {
    selected: 0,
    color: "#969ca8",
    selectedColor: "#3072f6",
    list: [
      {
        pagePath: "/pages/index/index",
        iconPath: "/assert/img/tab_home_normal.png",
        selectedIconPath: "/assert/img/tab_home_selected.png",
        text: "首页"
      },
      {
        pagePath: "/pages/exam/exam",
        iconPath: "/assert/img/tab_examination_normal.png",
        selectedIconPath: "/assert/img/tab_examination_selected.png",
        text: "考试"
      },
      {
        pagePath: "/pages/rdv/rdv",
        iconPath: "/assert/img/tab_class_normal.png",
        selectedIconPath: "/assert/img/tab_class_selected.png",
        text: "约课"
      },
      {
        pagePath: "/pages/user/user",
        iconPath: "/assert/img/tab_account_normal.png",
        selectedIconPath: "/assert/img/tab_account_selected.png",
        text: "我的"
      },
    ]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      if (e.currentTarget.dataset.index !== 0 && !getApp().globalData.login.token){
        wx.showToast({
          title: '登录中，请稍候重试',
          icon: 'none',
          image: '',
          duration: 1800,
          mask: true,
        })
        wx.switchTab({
          url: '/pages/index/index',
        })
        return;
      }
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({ url })
      this.setData({
        selected: data.index
      })
    }
  }
})