module.exports.areaList = [{
  province: '广西壮族自治区',
  provinceCode: 450000,
  city: [{
    name: '南宁市',
    code: 450100,
    area: [
      // {
      //   areaName: '市辖区',
      //   code: 450101
      // },
      {

        areaName: '兴宁区',
        code: 450102
      },
      {
        areaName: '青秀区',
        code: 450103
      },
      {
        areaName: '江南区',
        code: 450105
      },
      {
        areaName: '西乡塘区',
        code: 450107
      },
      {
        areaName: '良庆区',
        code: 450108
      },
      {
        areaName: '邕宁区',
        code: 450109
      },
      {
        areaName: '武鸣县',
        code: 450122
      },
      {
        areaName: '隆安县',
        code: 450123
      },
      {
        areaName: '马山县',
        code: 450124
      },
      {
        areaName: '上林县',
        code: 450125
      },
      {
        areaName: '宾阳县',
        code: 450126
      },
      {
        areaName: '横县',
        code: 450127
      }
    ]
  }]
}, ]