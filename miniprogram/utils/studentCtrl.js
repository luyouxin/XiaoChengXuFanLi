const appGlobalData = getApp().globalData;
// 学员科目设置
module.exports.StudentSubjectCtrl = (subject, good, bad, done) => {
  wx.request({
    url: 'https://www.haitunxueche.com/student/up2Subject',
    data: {
      // 科目，登录信息为学生时有值，1-科目一，2-科目二，3科三，4-科四，5-毕业，11-体检，12-面签, 13-预约科目一
      subject:subject,
    },
    header: {
      Authorization: appGlobalData.login.token,
    },
    method: 'GET',
    success: function(res) {
      if(res.data.code === 0) {
        good(res.data.data);
      }else{
        bad(res.data);
      }
    },
    fail: function(res) {
      bad(res);
    },
    complete: function(res) {
      if(done){
        done(res);
      }
    },
  })
}

module.exports.ToStudentReq = (id, good, bad, done) => {
  let _url = 'https://www.haitunxueche.com/student/toStudentReq?teacherOpenId=' + id;
  // console.log(_url)
  wx.request({
    url: _url,
    data: '',
    header: {
      Authorization: appGlobalData.login.token,
    },
    method: 'POST',
    success: function(res) {
      if(res.data.code === 0) {
        good(res.data.data);
      }else{
        bad(res.data);
      }
    },
    fail: function(res) {
      bad(res);
    },
    complete: function(res) {
      if(done){
        done(res);
      }
    },
  })
}

module.exports.ReqStudent = (data, good, bad, done) => {
  let str = '';
  for(let i in data){
    str += i + '=' + data[i] + '&';
  }
  str = str.slice(0,str.length -1);
  console.log(str)
  let _url = 'https://www.haitunxueche.com/student/reqStudent?' + str;
  wx.request({
    url: _url,
    data: '',
    header: {
      Authorization: appGlobalData.login.token,
    },
    method: 'POST',
    success: function(res) {
      if(res.data.code === 0) {
        good(res.data.data);
      }else{
        bad(res.data);
      }
    },
    fail: function(res) {
      bad(res);
    },
    complete: function(res) {
      if(done){
        done(res);
      }
    },
  })
}

module.exports.withdraw = (data, good, bad, done) => {
  let _url = 'https://www.haitunxueche.com/finance/withdraw?amount=' + data;
  wx.request({
    url: _url,
    data: '',
    header: {
      Authorization: appGlobalData.login.token,
    },
    method: 'GET',
    success: function(res) {
      if(res.data.code === 0) {
        good(res.data.data);
      }else{
        bad(res.data);
      }
    },
    fail: function(res) {
      bad(res);
    },
    complete: function(res) {
      if(done){
        done(res);
      }
    },
  })
}