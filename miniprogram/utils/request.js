const baseUrl = 'https://www.haitunxueche.com/';

module.exports.request = function (method, url, data, success, fail, complete) {
  let token;
  if (getApp().globalData.login) {
    token = getApp().globalData.login.token;
  }
  if (!token) {
    throw ('non valide token');
    return;
  }
  wx.request({
    url: baseUrl + url,
    method: method,
    data: data,
    header: {
      Authorization: token,
    },
    success: function (d) {
      if (success) {
        success(d);
      }
    },
    fail: function (e) {
      if (fail) {
        fail(e);
      }
    },
    complete: function () {
      if (complete) {
        complete();
      }
    }
  })
}

// 获取用户提现记录
module.exports.pageStudentWithdraw = function ( page = 1, pageSize = 30, success, fail, complete) {
  wx.request({
    url: baseUrl + 'finance/pageSchoolWithdraw',
    method: 'GET',
    data: {
      page: page,
      pageSize: pageSize,
    },
    header: {
      Authorization: getApp().globalData.login.token,
    },
    success: function (d) {
      if (d.data.code === 0) {
        success(d.data.data);
      } else {
        if(fail){
          fail(d.data)
        }
      }
    },
    fail: function (e) {
      if (fail) {
        fail(e);
      }
    },
    complete: function () {
      if (complete) {
        complete();
      }
    }
  })
}
