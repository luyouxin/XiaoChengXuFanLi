


/*
 * 教练端 nav 数据
 */

function getCoachNavData(){
  var arr = [
    {
      id: 1,
      title: "详情"
    },
    {
      id: 2,
      title: "评价"
    }
  ]
  return arr
}
function evaluateData(){
  var obj = {
    grade: 4.8,
    active: 3,
    activeHalf: 1,
    noActive: 1,
    evaluateRecord: 3,
    goodReputation: 98,
    mediumReputation:  6,
    BadReputation: 2,   
  }
  return obj
}
function slideTabsData(){
 
   var tabsList = [{
      "name": "全部",
      "count": 3,
    },{
      "name": "好评",
      "count": 1,
    },{
      "name": "中评",
      "count": 1,
    },{
      "name": "差评",
      "count": 1,
    }]
  
  return tabsList
}
function commentData(){
  var commentList = [{
    "portrait": "../assert/img/man_pic.jpg",
    "fulstar": 4,
    "graystar": 1,
    "time": "2018.09.03",
    "content": "老师人很好!",
  },{
    "portrait": "../assert/img/man_pic.jpg",
    "fulstar": 4,
    "graystar": 1,
    "time": "2018.09.03",
    "content": "老师人很好!",
  }]
  return commentList
}
/*
 * 我的 navnav 数据
 */
function getIndexNavData() {
  var arr = [
    {
      id: 1,
      icon: "../../assert/img/nav_icon_01.png",
      title: "订单"
    },
    {
      id: 2,
      icon: "../../assert/img/nav_icon_02.png",
      title: "钱包"
    },
    {
      id: 3,
      icon: "../../assert/img/nav_icon_03.png",
      title: "消息"
    },
    {
      id: 4,
      icon: "../../assert/img/nav_icon_04.png",
      title: "收藏"
    },
    {
      id: 5,
      icon: "../../assert/img/nav_icon_05.png",
      title: "评价"
    },
    {
      id: 6,
      icon: "../../assert/img/nav_icon_06.png",
      title: "优惠券"
    },
    {
      id: 7,
      icon: "../../assert/img/nav_icon_07.png",
      title: "客服"
    },
    {
      id: 8,
      icon: "../../assert/img/nav_icon_08.png",
      title: "设置"
    }
  ]
  return arr
}

/**
 * 对外暴露接口
 */
module.exports = {
  getIndexNavData: getIndexNavData,
  getCoachNavData: getCoachNavData,
  evaluateData: evaluateData,
  slideTabsData: slideTabsData,
  commentData: commentData,
}