
module.exports.loginFunc = function(resolv,rejcet) {
  return new Promise(function(resolv,rejcet){
    wx.login({
      success: function(res) {
        resolv(res);
      },
      fail: function(res) {
        rejcet(res);
      },
      complete: function(res) {},
    })
  })
}

module.exports.requestToken = function(wxcode,resolv,rejcet) {
  return new Promise(function(resolv,rejcet){
    wx.request({
      url: 'https://www.haitunxueche.com/user/authWx',
      data: {
        code:wxcode,
        loginType:'STUDENT'
      },
      method: 'GET',
      success: function(res) {
        resolv({
          res:res,
          wxcode: wxcode,
        });
      },
      fail: function(res) {
        rejcet(res);
      },
      complete: function(res) {},
    })
  })
}

module.exports.LoginWithPhone = function(d,resolv,reject){
  return new Promise(function (resolv, reject){
    const _app = getApp();
    wx.request({
      url: 'https://www.haitunxueche.com/user/login',
      method: 'GET',
      data: {
        code: _app.globalData.login.wxCode,
        encryptedData: d.encryptedData,
        iv: d.iv,
        loginType: 'STUDENT',
        openId: _app.globalData.login.openId,
        sessionKey: _app.globalData.login.sessionKey,
      },
      success(d) {
        console.log(d);
        // 后续操作,需要测试
        if (d.data.code === 0) {
          const _app = getApp();
          _app.globalData.login = {
            openId: d.data.data.openId,
            sessionKey: d.data.data.sessionKey,
            token: d.data.data.token,
          }
          resolv('sucess');
        } else {          
          reject(d.data);
        }
      },
      fail(e) {
        reject(e)
      }
    });
  })
}

// 上传更新用户信息
module.exports.getUserStudyInfo = function(token,data,resolv,reject){
  return new Promise((resolv,reject) =>{    
    wx.request({
      url: 'https://www.haitunxueche.com/student/updateStudentInfo',
      data: data,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      success: function(res) {
        resolv(res);
      },
      fail: function(res) {
        reject(res);
      },
      complete: function(res) {},
    })
  })
}
