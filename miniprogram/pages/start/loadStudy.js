// 1-科目一，2-科目二，3科三，4-科四，5-毕业，11-体检，12-面签

module.exports = function (resolv, reject) {
  return new Promise(function (resolv, reject) {
    if(!getApp().globalData.userInfo.subject){
      // 没有用户班级信息，不做请求
      return false;
    }
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/getClasses',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: function (res) {
        // resolv(res);
        if (res.data.code === 0) {
          console.log(res);
          getApp().globalData.userStudyInfo = res.data.data;
        } else {
          getApp().globalData.userStudyInfo = null;
        }
      },
      fail: function (res) {
        getApp().globalData.userStudyInfo = null;
      },
      complete: function (res) { },
    })
  })
}