// pages/user/user.js
//获取应用实例
var app = getApp()
// var navData = require('../../utils/data.js')
Page({
  data: {
    modalCtrl: 'hidden',
    avatarUrl: '',
    nickName: '',
    myCoach: '',
    remainClass: 0,
    shareCount: 0,
  },
  onLoad: function() {
    var log = console.log.bind(console);
    let userInfo = app.globalData.userInfo;
    const avatarImg = userInfo.avatarUrl || app.globalData.userWxInfo.avatarUrl || '';
    if (userInfo.nickName == null) {
      this.setData({
        avatarUrl: avatarImg,
        nickName: app.globalData.userWxInfo.nickName,
      });
    } else {
      this.setData({
        avatarUrl: avatarImg,
        nickName: userInfo.nickName,
      });
    }
    this.getSharePromise().then(res => {
      if (res.data.code == 0 && res.data.message == '成功') {
        this.setData({
          shareCount: res.data.data.shareCount,
        })
      }
    }).catch(err => {
      console.error(err);
    })
  },
  onShow() {
    let _userSubject = app.globalData.userStudyInfo;
    if (_userSubject && _userSubject.subject === 2) {
      this.setData({
        userSubject: '科二'
      });
    } else if (_userSubject && _userSubject.subject === 3) {
      this.setData({
        userSubject: '科三'
      });
    }
    this.getRemainClass().then(res => {
      if (res.data.code == 0 && res.data.message == "成功") {
        let obj = res.data.data;
        if (obj.subject == 2 || obj.subject == 3) {
          let _remainClass = parseInt(obj.totalClass - obj.overClass);
          // console.log(_remainClass);
          this.setData({
            remainClass: _remainClass,
          })
        }
      }
    }).catch(err => {
      console.warn(err);
    });
  },
  onReady: function() {
    this.didIHaveCoach();
    this.getMyMoneyRest();
    ///
    wx.cloud.callFunction({
      name: 'loaddb',
      success: (res) =>{
        console.log(res);
      },
      fail:(err) => {
        console.log(err);
      }
    })
    ///
  },
  didIHaveCoach() {
    return;
    if (app.globalData.userStudyInfo) {
      const info = app.globalData.userStudyInfo;
      if (info && info.teacherName && info.teacherId) {
        this.setData({
          myCoach: '我的教练：' + info.teacherName,
        })
      }
    } else {
      this.setData({
        myCoach: '请选择教练 >',
      })
    }
  },
  handleMyFee() {
    wx.navigateTo({
      url: '../myFee/myFee',
    })
  },
  handleInvited() {
    wx.navigateTo({
      url: '../successInvited/successInvited',
    })
  },
  handleMyOrder() {
    wx.navigateTo({
      url: '../myOrder/myOrder',
    })
  },
  handleSetting() {
    wx.navigateTo({
      url: '../mySetting/mySetting',
    })
  },
  handleEvaluate() {
    wx.navigateTo({
      url: '../myEvaluate/myEvaluate',
    })
  },
  handleMyCollect() {
    wx.navigateTo({
      url: '../myCollect/myCollect',
    })
  },
  handleMyShare() {
    wx.navigateTo({
      url: '../myShare/myShare',
    })
  },
  handleMyShare2() {
    wx.navigateTo({
      url: '../share/share',
    })
  },
  handleAppointment() {
    wx.setStorageSync('rdvpage2', true);
    wx.switchTab({
      url: '../rdv/rdv',
    })
  },
  handleSevice() {
    this.setData({
      modalCtrl: 'show'
    })
  },
  onCloseEvent() {
    this.setData({
      modalCtrl: 'hidden'
    })
  },
  toweb: function(e) {
    const str = e.currentTarget.dataset.page;
    const time = e.currentTarget.dataset.time;
    wx.navigateTo({
      url: '../articleReading/articleReading?id=' + str + '&time=' + time,
    })
  },
  handleCoach(e) {
    console.log(e)
    if (e.currentTarget.dataset.text.indexOf('请选择') >= 0) {
      wx.switchTab({
        url: '../index/index',
        // url: '../allCoach/allCoach?selected=2',
      })
    } else {
      wx.navigateTo({
        url: '../coach/coach?tid=' + app.globalData.userStudyInfo.teacherId,
      })
    }
  },
  handleContact(e) {
    console.log(e.path)
    console.log(e.query)
  },
  getRemainClass() {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getClasses',
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
          if(res.data.code === 0){
            getApp().globalData.userStudyInfo = res.data.data;
            this.setData({ myCoach: '我的教练：' + res.data.data.teacherName, });
          }else{
            getApp().globalData.userStudyInfo = null;
            this.setData({ myCoach: '请选择教练 >', });
          }     
        },
        fail: (err) => {
          reject(err);
        },
        complete: (e) => {

        }
      })
    })
  },
  getSharePromise() {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/getShareVo',
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (err) => {
          reject(err);
        },
        complete: (e) => {

        }
      })
    })
  },
  cjwt() {
    wx.navigateTo({
      url: '../wenzhang/wenzhang?key=cjwt',
    })
  },
  callService() {
    wx.makePhoneCall({
      phoneNumber: '400',
      success: function(res) {},
      fail: function(res) {
        wx.showToast({
          title: '拨打电话失败',
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      },
      complete: function(res) {},
    })
  },
  getMyMoneyRest(){
    wx.request({
      url: getApp().globalData.serverAddress + 'finance/getFinanceVo',
      data: '',
      header: {
        Authorization : getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res)=> {
        if(res.data.code === 0){
          this.setData({
            restMoney: res.data.data.amount.toFixed(2),
          })
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  }
})