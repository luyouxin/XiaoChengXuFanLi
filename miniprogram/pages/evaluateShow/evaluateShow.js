// pages/evaluateShow/evaluateShow.js
Page({

  data: {
    test: false,
    className: '',
    odId: '',
    teacherName: '',
    topPic: '',
    star: 5,
    grayStar: 0,
    one_2: 0,
    two_2: 5,
    flag: [],
    flag2: [],
    score: 5,
    score1: 5,
    isTrue: false,
    textAreaVal: '',
    checkboxArr: [{
      name: '责任心强',
      checked: false
    }, {
      name: '平易近人',
      checked: false
    }, {
      name: '专业度高',
      checked: false
    }, {
      name: '一级教练',
      checked: false
    }],
    checkboxArr3: [{
      name: '不负责任',
      checked: false
    }, {
      name: '态度不好',
      checked: false
    }, {
      name: '经常失约',
      checked: false
    }, {
      name: '不够专业',
      checked: false
    }],
  },

  onLoad: function(options) {
    console.log(options);
    this.getEvaluatePromise(options.orderId).then(res => {
      console.info(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        this.setData({
          className: res.data.data.className,
          odId: res.data.data.odId,
          teacherName: res.data.data.teacherName,
          topPic: res.data.data.topPic,
        })
      }
    }).catch(err => {
      console.warn(err);
    })
  },
  onReady: function() {

  },
  onUnload(){
    // this.data.test = true;
    // console.log(this.data.test);
    // console.log(getCurrentPages().length == 3);
    // if(getCurrentPages().length == 3){
    //   this.setData({
    //     test: true,
    //   })
    // }
  },
  closeModalFunc() {
    this.setData({
      test: false,
    })
  },
  yesFun() {
    this.setData({

    })
  },
  getEvaluatePromise(_orderId) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/toEvaluate',
        data: {
          id: _orderId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: (e) => {
          console.log(e);
        }
      })
    })
  },
  submitPromise(_anonymous, _mark, _odId, _remarks, _score) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/addEvaluate',
        data: {
          anonymous: _anonymous,
          mark: _mark,
          odId: _odId,
          remarks: _remarks,
          score: _score,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'POST',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: (e) => {
          console.log(e);
        }
      })
    })
  },
  handleFlag(e) {
    console.log(e);
    const index = e.target.dataset.index;
    var checkboxArr = this.data.checkboxArr; //选项集合
    checkboxArr[index].checked = !checkboxArr[index].checked; //改变当前选中的checked值
    console.log(checkboxArr);
    this.setData({
      checkboxArr: checkboxArr,
      flag2: [],
    })
    console.log(this.data.flag2);
    //筛选出为真的
    const checkboxArr2 = this.data.checkboxArr.filter(v => {
      return v.checked == true;
    })
    console.log(checkboxArr2);
    //过滤掉name,checked
    const _arr = [];
    for (let i of checkboxArr2) {
      let _obj = Object.assign({}, i);
      delete _obj.checked;
      console.info(_obj);
      _arr.push(...Object.values(_obj));
      this.setData({
        flag: _arr,
      });
    }
    console.log(_arr);
    console.log(this.data.flag);
    //判断两个some
    let trueFlag = this.data.checkboxArr.some(v => {
      return v.checked == true;
    })
    console.log(trueFlag);
    if (trueFlag) {
      this.data.checkboxArr3.forEach((v, k, s) => {
        if (v.checked == true) {
          v.checked = false;
        }
        console.log(v, k, s);
        this.setData({
          checkboxArr3: this.data.checkboxArr3,
        })
      }, this.data.checkboxArr3)
    }
  },
  handleFlag2(e) {
    console.log(e);
    const index = e.target.dataset.index;
    var checkboxArr3 = this.data.checkboxArr3; //选项集合
    checkboxArr3[index].checked = !checkboxArr3[index].checked; //改变当前选中的checked值
    console.log(checkboxArr3);
    this.setData({
      checkboxArr3: checkboxArr3,
      flag: [],
    })
    console.log(this.data.flag);
    //筛选出为真的
    const checkboxArr4 = this.data.checkboxArr3.filter(v => {
      return v.checked == true;
    })
    console.log(checkboxArr4);
    //过滤掉name,checked
    const _arr = [];
    for (let i of checkboxArr4) {
      let _obj = Object.assign({}, i);
      delete _obj.checked;
      console.info(_obj);
      _arr.push(...Object.values(_obj));
      this.setData({
        flag2: _arr,
      });
    }
    console.log(_arr);
    console.log(this.data.flag2);
    //判断两个some
    let trueFlag = this.data.checkboxArr3.some(v => {
      return v.checked == true;
    })
    console.log(trueFlag);
    if (trueFlag) {
      this.data.checkboxArr.forEach((v, k, s) => {
        if (v.checked == true) {
          v.checked = false;
        }
        console.log(v, k, s);
        this.setData({
          checkboxArr: this.data.checkboxArr,
        })
      }, this.data.checkboxArr)
    }
  },
  handleisTrue(e) {
    this.setData({
      isTrue: !this.data.isTrue,
    })
  },
  handleSubmit(e) {
    let isTrue = this.data.isTrue;
    let mark = '';
    if (this.data.flag.length == 0) {
      mark = this.data.flag2.toString();
    } else if (this.data.flag2 == 0) {
      mark = this.data.flag.toString();
    }
    // let mark = (this.data.flag).toString();
    // let mark = '非常好,很好';
    let odId = this.data.odId;
    let remarks = this.data.textAreaVal;
    let score = this.data.score1;
    console.log(isTrue, mark, odId, remarks, score);
    this.submitPromise(isTrue, mark, odId, remarks, score).then(res => {
      console.info(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        wx.showToast({
          title: '评价提交成功',
          icon: "loading"
        })
        setTimeout(function() {
          wx.navigateBack({
            delta: 1,
          })
          getCurrentPages()[1].onLoad();
        }, 2200);
      }
    }).catch(err => {
      console.warn(err);
    })
  },
  selectIndexNum(e) {
    let i = e.currentTarget.dataset.index;
    console.log(i);
    if (i == 0) {
      this.setData({
        score: 0
      })
    } else if (i == 4) {
      this.setData({
        score: 5
      })
    } else {
      this.setData({
        score: e.currentTarget.dataset.index - 0
      })
    }
    console.log(this.data.score);
    if (i == 0) {
      this.setData({
        score1: 1,
      })
    } else if (i == 1) {
      this.setData({
        score1: 2,
      })
    } else if (i == 2) {
      this.setData({
        score1: 3,
      })
    } else if (i == 3) {
      this.setData({
        score1: 4,
      })
    } else if (i == 4)(
      this.setData({
        score1: 5,
      })
    )
    console.log(this.data.score1);
  },
  handleTextarea(e) {
    console.log(e);
    let value = e.detail.value;
    console.log(value);
    this.setData({
      textAreaVal: value,
    })
  }
})