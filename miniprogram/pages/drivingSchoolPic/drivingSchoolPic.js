// pages/drivingSchoolPic/drivingSchoolPic.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    strArr: [],
    strArrLength: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.picArr);
    console.log(typeof(options));
    console.log(Object.values(options)[0].split());
    let strArr = (options.picArr).split(',');
    console.log(strArr);
    this.setData({
      strArr: strArr,
      strArrLength: strArr.length,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  openBig(e){
    const url = e.currentTarget.dataset.src;
    console.log(url);
    let urls = [];
    for (let i of this.data.strArr){
      urls.push('https://htxc.oss-cn-shenzhen.aliyuncs.com' + i);
    }
    wx.previewImage({
      current: url,
      urls: urls,
    })
  }
})