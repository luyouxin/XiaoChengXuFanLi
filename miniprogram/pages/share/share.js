// pages/share/share.js
Page({

  data: {

  },
  onLoad: function (options) {

  },
  onReady: function () {
    this.getWXQrcode();
  },
  onShareAppMessage(){
    wx.showShareMenu({
      withShareTicket: true, 
    })
    return {
      title: '邀请好友',
      path: '/pages/index/index?sharefromuserid=' + getApp().globalData.login.openId,
    }
  },
  getWXQrcode(){
    wx.request({
      url: getApp().globalData.serverAddress + 'student/getWxaCodeUnlimit',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        this.setData({
          wxQrcode: getApp().globalData.ossLink + res.data.data,
        })
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  }
})