const userModule = require('../../utils/usermodule.js');

const app = getApp()
const query = wx.createSelectorQuery();

const loader = require('teacherSchool.js');

Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    showCoachList: true,
    hiddeScroll: true,
    upperTrigger: 0,
    showFixed: false,
    filterOrder: 'DISTANCE',
    jlList: null,
    jsHeight: 'small',
    blockScrollHandle: false,
    // testHeight: null,
    loadingJLDone: false,
    currentCity: '城市',
    notionMsg: '载入',
    authrization: false, // 询问权限用
    cityFilter: false, // 是否根据城市筛选
    requestTarget: '教练',
    sexFilter: null, /// 性别筛选
  },
  onShow: function () {
    this.startPageLogic();
  },
  onLoad: function () {
    this._observer = wx.createIntersectionObserver(this) // 筛选栏显示
    this._observer
      .relativeTo('.triggerFixed')
      .observe('.jl-show', (res) => {
        this.setData({
          appear: res.intersectionRatio > 0
        })
      });
    this.loader = new loader(this);
  },
  onReady() {
   
  },
  getPhone(e) { // 电话登陆
    if (e.detail.detail.errMsg.indexOf('deny' >= 0)) {
      this.userInstance.bindUserPhone(e.detail.detail);
    }
  },
  withoutPhone() {
    this.userInstance.bindUserPhone(false);
  },
  startPageLogic: function () {
    // console.log('初始化结束，开始页面逻辑');
    this.loader.requestData();
  },
  failToLocateCity: function (e) {  // 不能进入站点页面
    console.warn(e);
    wx.navigateTo({ // 跳转选择站点
      url: '../citySelect/citySelect?location=fail',
    })
  },
  jump: function (e) {
    wx.navigateTo({
      url: '../home/home',
    })
  },
  //聚焦跳转页面
  focusEvent(e) {
    wx.navigateTo({
      url: '../citySelect/citySelect',
    })
  },
  changeOrder(e) { // 修改排序方法
    if (this.data.showCoachList) { // 只有老师有排序
      this.data.teacherOptionsNext.orderBy = e.currentTarget.dataset.name;
      this.setData({
        teacherOptionsNext: this.data.teacherOptionsNext,
        filterOrder: e.currentTarget.dataset.name,
      })
      this.loader.requestData();
    }
  },
  showFilter(e) { // 跳转筛选页面
    wx.navigateTo({
      url: '../filter/filter',
    })
  },
  reLoadWithFilter: function (params) { // 筛选方法
    if (params) {
      this.setData({
        extraParams: params,
      })
    } else {
      this.setData({
        extraParams: null,
      })
    }
    this.loader.requestData();
  },

  changeData: function (e) {
    let coach ;
    if (e.detail.text === '教练'){
      coach = true;
    }else{
      coach = false;
    }
    this.setData({
      showCoachList: coach,
      requestTarget: coach ? '教练' : '驾校',
      filterOrder: 'DISTANCE',
    })
    this.loader.requestData();
  }
})