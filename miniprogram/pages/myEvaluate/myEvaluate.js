// pages/myEvaluate/myEvaluate.js
Page({

  data: {
    starsArr: [2, 2, 2, 1, 0],
    //是否评价 0 1
    isEvaluation: 1,
    evaluateList: [],
    evaluateListLength: 0,
    overClass: 0,
    totalClass: 0,
    page: 1,
    size: 50,
    loading: true,
    hasMore: true,
  },

  onShow: function(options) {
    this.getEvaluatePromise(this.data.page, this.data.size).then(res => {
      // console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        //筛选出状态为5的状态或者为2的状态功能
        const newArr = res.data.data.records;

        const newArr2 = newArr.filter(v => {
          //转换时间戳 && 获取当前时间戳
          v.time = v.bespokenDate + 1 * 3600 * 1000;
          let timeStamp = new Date().getTime();
          console.log(this.timestampToTime(v.time));
          console.log(timeStamp > v.time);
          return (v.orderStatus == 5 && timeStamp > v.time) || (v.orderStatus == 2 && timeStamp > v.time);
        })
        console.log(newArr2);
        //处理时间戳
        const _arr = [];
        for (let i of newArr2) {
          let o = Object.assign({}, i);
          o.time = this.timestampToTime(i.createDate);
          let weekArr = this.timestampToTime(i.createDate).split(' ')[0];
          let weekArr1 = this.timestampToTime(i.createDate).split(' ')[1].split(":").slice(0, 2).join(":");
          o.week2 = this.getMyDay(new Date(weekArr));
          o.week3 = weekArr1;
          o.allStars = parseInt(i.evaluateScore);          
          o.grayStars = 5 - o.allStars;
          o.orderStatus = i.orderStatus == 2 ? "已支付" : "已完成";
          _arr.push(o);
        }
        this.setData({
          evaluateList: _arr,
          evaluateListLength: _arr.length,
          // evaluateList: res.data.data.records,
        })

      }
    }).catch(error => {
      console.warn(error);
    });
    this.getRemainClass().then(res => {
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        let obj = res.data.data;
        if (obj.subject == 2 || obj.subject == 3) {
          this.setData({
            overClass: obj.overClass,
            totalClass: obj.totalClass,
          })
        }
      }
    }).catch(err => {
      console.warn(err);
    });
  },
  onReady: function() {

  },
  handleEvaluate(e) {
    console.log(e);
    const oid = e.currentTarget.dataset.oid
    wx.navigateTo({
      url: '../evaluateShow/evaluateShow?orderId=' + oid,
    })
  },
  getEvaluatePromise(_current, _page) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/pageBespokenDetailVo',
        data: {
          current: _current,
          page: _page,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function() {}
      })
    })
  },
  getRemainClass() {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getClasses',
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (err) => {
          reject(err);
        },
        complete: (e) => {

        }
      })
    })
  },
  timestampToTime(timestamp) {
    // var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    // var D = date.getDate() + ' ';
    var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return Y + M + D + h + m + s;
  },
  getMyDay(date) {
    var week;
    if (date.getDay() == 0) week = "周日"
    if (date.getDay() == 1) week = "周一"
    if (date.getDay() == 2) week = "周二"
    if (date.getDay() == 3) week = "周三"
    if (date.getDay() == 4) week = "周四"
    if (date.getDay() == 5) week = "周五"
    if (date.getDay() == 6) week = "周六"
    return week;
  },
  loadMore() {
    if (!this.data.hasMore) return;
    //渲染数据
    this.getEvaluatePromise(++this.data.page, this.data.size).then(res => {
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        //筛选出状态为5的状态或者为2的状态功能
        const newArr = res.data.data.records;
        if (newArr.length) {
          const newArr2 = newArr.filter(v => {
            //转换时间戳 && 获取当前时间戳
            v.time = v.bespokenDate + 1 * 3600 * 1000;
            let timeStamp = new Date().getTime();
            console.log(this.timestampToTime(v.time));
            console.log(timeStamp > v.time);
            return (v.orderStatus == 5 && timeStamp > v.time) || (v.orderStatus == 2 && timeStamp > v.time);
          })
          console.log(newArr2);
          //处理时间戳
          const _arr = [];
          for (let i of newArr2) {
            let o = Object.assign({}, i);
            o.time = this.timestampToTime(i.createDate);
            let weekArr = this.timestampToTime(i.createDate).split(' ')[0];
            let weekArr1 = this.timestampToTime(i.createDate).split(' ')[1].split(":").slice(0, 2).join(":");
            console.log(weekArr, weekArr1);
            console.log(weekArr);
            o.week2 = this.getMyDay(new Date(weekArr));
            o.week3 = weekArr1;
            o.allStars = 5;
            o.grayStars = 5 - parseInt(i.evaluateScore);
            o.orderStatus = i.orderStatus == 2 ? "已支付" : "已完成";
            _arr.push(o);
          }
          this.setData({
            evaluateList: _arr,
            evaluateListLength: _arr.length,
          })
        } else {
          this.setData({
            hasMore: false,
            loading: false,
          })
        }

      }
    }).catch(error => {
      console.warn(error);
    });
    this.getRemainClass().then(res => {
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        let obj = res.data.data;
        if (obj.subject == 2 || obj.subject == 3) {
          this.setData({
            overClass: obj.overClass,
            totalClass: obj.totalClass,
          })
        }
      }
    }).catch(err => {
      console.warn(err);
    });

  },
  onReachBottom() {
    this.loadMore();
  }
})