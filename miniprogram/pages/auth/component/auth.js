// pages/index/component/auth.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    auth: String,
  },

  /**
   * 组件的初始数据
   */
  data: {
    authPhoneBtn: true,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getUInfo(e) {
      // console.log(e);
      this.triggerEvent('getUInfo', {}, {});
    },
    getPhone(e) {
      // console.log(e);
      this.triggerEvent('getPhone', {
        detail: e.detail
      }, {});
      this.hiddePhoneBtn();
      setTimeout(() => {
        this.showPhoneBtn();
      }, 5000)
    },
    hiddePhoneBtn() {
      this.setData({
        authPhoneBtn: false,
      })
    },
    showPhoneBtn() {
      this.setData({
        authPhoneBtn: true,
      })
    },
    withoutPhone(){
      this.triggerEvent('withoutPhone',{},{});
    }
  }
})
