const initUser = require('../../utils/initUser.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    this.setData({
      authrization: options.for,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  getUInfo() {
    // wx.switchTab({
    //   url: '../index/index',
    // })
    // console.log(getApp())
    initUser(getApp());
  },
  withoutPhone() {
    // wx.switchTab({
    //   url: '../index/index',
    // })
    initUser(getApp());
  },
  getPhone(d) {
    wx.showLoading({
      title: '',
      mask: true,
    })
    wx.request({
      url: 'https://www.haitunxueche.com/user/bingPhone',
      data: {
        encryptedData: d.detail.detail.encryptedData,
        iv: d.detail.detail.iv,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code === 0) {
          // wx.switchTab({
          //   url: '../index/index',
          // });
          getApp().globalData.userInfo = res.data.data;
          initUser(getApp());
        };
      },
      fail: (res) => {
        wx.showModal({
          title: '绑定手机失败',
          content: JSON.stringify(res.data),
          showCancel: false,
          confirmText: '重试',
          confirmColor: '',
        })
      },
      complete(){
        wx.hideLoading();
      }
    })
  }

})