Component({
  options:{
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
   //组件的对外属性，属性设置中可包含三个字段,type 表示属性类型、 value 表示属性初始值、 observer 表示属性值被更改时的响应函数
   properties: {
     grade:{
      type: String,
      value: '',
       observer(newVal, oldVal, changedPath){

       }
     },
     active:{
       type: Number,
       value: 5,
     },
     activeHalf:{
       type: Number,
       value: 2,
     },
     noActive:{
       type: Number,
       value: 1,
     },
     evaluateRecord:{
       type: Number,
       value: 1326,
     },
     badRate: {
       type: Number,
       value: 0,
     },
     midRate: {
       type: Number,
       value: 0,
     },
     goodRate: {
       type: Number,
       value: 0,
     }
   },
   //组件的内部数据，和 properties 一同用于组件的模版渲染
   data: {
    //  grade: 5.0,
   },
   //组件的方法，包括事件响应函数和任意的自定义方法
   methods: {

   },
   ready(){
    //  console.log(this.properties.grade);
    // this.properties.grade.toFixed(1); 
   }
})