// pages/component/coach/coach_block.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    coach: Object,
    sex: Number,
  },

  /**
   * 组件的初始数据
   */
  data: {

  },
  
  methods: {
    toDetail(e){      
      wx.navigateTo({
        url: '../coach/coach?tid=' + e.currentTarget.dataset.tid,
      })
    },
  }
})
