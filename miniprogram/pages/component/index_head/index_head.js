// pages/component/index_head.js


Component({
  /**
   * 组件的属性列表
   */
  properties: {    
    city:{
      type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function (newVal, oldVal, changedPath) {
        this.setData({
          currentCity:newVal,
        })
      }
    }
  },
  lifetimes:{
    ready:function(){
      
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    index:['123','123'],
    currentCity:'',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    toFilter:function(e){
      wx.navigateTo({
        url: '../search/search',
      });
    },
    toPage:function(e){
      console.log(e.currentTarget.dataset);
      if ('url' in e.currentTarget.dataset){
        wx.navigateTo({
          url: e.currentTarget.dataset.url,
        });
      }
    },
    tijian:function(e){
      wx.navigateTo({
        url: '../tijian/tijian',
      })
    },
    changeCity:function(e){
      wx.navigateTo({
        url: '../citySelect/citySelect',
      });
    },
    changeNav:function(e){
      // console.log(e);
      this.triggerEvent("changeNavHead",{
        text:e.detail.text,
      },{
        bubble:true,
      })
    },
    toweb:function(e){
      const str = e.currentTarget.dataset.page;
      const time = e.currentTarget.dataset.time;
      wx.navigateTo({
        url: '../articleReading/articleReading?id='+str + '&time='+ time,
      });
    },
    jssf:function(){
      wx.navigateTo({
        url: '../courDetail/courDetail?jssf',
      });
    },
    toEnsurance() {
      wx.navigateTo({
        // url: '../courDetail/courDetail?baoxian',
        url:'/pages/insurance/insurance',
      })
    },
    xclc(){
      wx.navigateTo({
        url: '../wenzhang/wenzhang?page=xclc',
      })
    }
  }
})
