/** 
  commentData 类型说明:{
   totalPoint 总评分,总评分必须是0-5范围,
   good: 好评总数,
   commonly:中评总数,
   bad: 差评总数,
   commentContent: 评论列表数据,
   commentList: [{ 具体的单条评论数组
    userName: 用户名字,
    userPhoto: 用户头像,
    userPoint: 用户评分,输入 0-5 数字,
    date: 评论时间，需要传入时间的字符串，不做转换,
    remark: 评论文字,
   }]
  }
  loading: boolean , 显示正在加载,
*/

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    commentData: {
      type: Object,
      value: '',
      observer(n, o, p) {
        if (n == null) {
          this.setData({
            totalPoint: '0.0'
          });
          return;
        }

        if (n.totalPoint && Number(n.totalPoint)) {
          this.setData({
            totalPoint: Number(n.totalPoint).toFixed(1).toString()
          });
        } else {
          this.setData({
            totalPoint: '0.0'
          });
        }
        if (true) { // 统计评论数据
          let _total = Math.ceil(n.good + n.commonly + n.bad);
          if (_total === 0) {
            this.setData({
              good: Number(n.good).toFixed(0),
              commonly: Number(n.commonly).toFixed(0),
              bad: Number(n.bad).toFixed(0),
              totalComment: _total,
              goodRatio: '0.0',
              commonlyRatio: '0.0',
              badRatio: '0.0',
            });
          } else {
            this.setData({
              good: Number(n.good).toFixed(0),
              commonly: Number(n.commonly).toFixed(0),
              bad: Number(n.bad).toFixed(0),
              totalComment: _total,
              goodRatio: Number(n.good / _total * 100).toFixed(1),
              commonlyRatio: Number(n.commonly / _total * 100).toFixed(1),
              badRatio: Number(n.bad / _total * 100).toFixed(1),
            });
          }
        }
        this.handleTotalStar();
        this.commentStarsHandle(n);
      }
    },
    loading: {
      type: Boolean,
      observer(n, o, p) {
        this.setData({
          loading: n || false,
        })
      }
    }
  },

  data: {
    totalPoint: '0.0',
    good: 0,
    commonly: 0,
    bad: 0,
    totalComment: 0,
    goodRatio: 0,
    commonlyRatio: 0,
    badRatio: 0,
    haveMore: false,
    loading: false, // 控制读取状态
    commentType: '全部',
    totalPointStars: ['empty', 'empty', 'empty', 'empty', 'empty'],
    commentList: [],
  },

  methods: {
    handleTotalStar() {
      let fullStar = Math.floor(Number(this.data.totalPoint));
      let restStar = Number(this.data.totalPoint) % 1;
      restStar = restStar >= 0.5 ? 'full' : 'half';
      let list = ['empty', 'empty', 'empty', 'empty', 'empty'];
      let i = 0;
      for (; i < fullStar; i++) {
        list[i] = 'full';
      }
      if (restStar > 0) {
        list[i] = restStar;
      }
      this.setData({
        totalPointStars: list
      });
    },
    commentStarsHandle(dat) {
      if (dat.commentList) {
        let list = dat.commentList;
        for (let i of list) {
          let tmp = ['empty', 'empty', 'empty', 'empty', 'empty'];
          if (i.userPoint) {
            let t = Number(i.userPoint);
            t = t > 5 ? 5 : t;
            t = t < 0 ? 0 : t;
            let fullStar = Math.floor(t);
            let restStar = t % 1;
            restStar = restStar >= 0.5 ? 'full' : 'half';
            let j = 0;
            for (; j < fullStar; j++) {
              tmp[j] = 'full';
            }
            if (restStar > 0) {
              tmp[j] = restStar;
            }
          }
          i.userPointStar = tmp;
        }
        this.setData({
          commentList: list,
        })
      }
    },
    changeType(e) {
      if (this.data.commentType == e.currentTarget.dataset.type) {
        return;
      }
      this.triggerEvent('changeType', {
        type: e.currentTarget.dataset.type
      }, {});
      this.setData({
        commentType: e.currentTarget.dataset.type
      });
    },
    loadmore(e){
      this.triggerEvent('loadmore', {}, {});
    }
  }
})