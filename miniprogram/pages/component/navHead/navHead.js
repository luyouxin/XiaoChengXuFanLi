// pages/component/navHead/navHead.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    navText: { // 属性名
      type: Array, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: '读取', // 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function (newVal, oldVal, changedPath) {
        // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
        // 通常 newVal 就是新设置的数据， oldVal 是旧数据
     }
    },   
  },

  /**
   * 组件的初始数据
   */
  data: {
    positon:'transform: translateX(0);',
    selected:'',
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    ready: function () { 
      this.setData({
        selected:this.data.navText[0], // 默认选第一个
      });
      const query = wx.createSelectorQuery().in(this);
      query.select('.nav-head').boundingClientRect( (rect) =>{
        this.triggerEvent('returnHeight', { headHeight: rect.height }, { bubbles: true }) 
      }).exec()
    },
    moved: function () { },
    detached: function () { },
  },

  /**
   * 组件的方法列表
   */
  methods: {    
    isActive:function(str){
      console.log(str,this.data.selected)
      if (str === this.data.selected){
        return 'name active';
      }else{
        return 'name'
      }
    },
    changeTitle:function(e){
      let targetText = e.currentTarget.dataset.text;
      this.changeBaseLinePositon(targetText);      
    },
    changeBaseLinePositon: function(choosedText){
      for (let i = 0; i < this.data.navText.length; i++) {
        if (this.data.navText[i] === choosedText) {
          let t = i * 130;
          let tt = t + 'rpx';
          this.setData({
            position: 'transform: translateX(' + tt + ');',
            selected: choosedText,
          });
          this.triggerEvent('changeNav', {text:choosedText}, { bubbles: true }) 
          break;
        }
      }
    }
  }
})
