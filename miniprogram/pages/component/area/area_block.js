// pages/component/area/area_block.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    area: Object,
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toDetail(e) {
      wx.navigateTo({
        url: '../drivingSchool/drivingSchool?tid=' + e.currentTarget.dataset.tid,
      })
    }
  }
})
