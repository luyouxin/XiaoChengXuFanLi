// pages/component/inputComponent/stdInputLeftIcon.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    icon:{
      type:String,
      value:'',
      observer:function(ne,old,d){
      }
    },
    pholder:{
      type:String,
      value:'请输入',
    },
    confirmType:{
      type:String,
      value:'done',
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    placeholderText:'',
    width:'',
  },
  attached: function () { 
    this.setData({
      placeholderText:this.data.pholder,
      confirmType: this.data.confirmType,
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    inputChange(e){
      // console.log(e.detail.value);
      this.triggerEvent('inputChange', e.detail.value, {
        bubbles:true,
      })
    }
  }

})
