// pages/component/modalComponent/modalComponent.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    ctrl2:{ // 控制显示的，当传入的ctrl改变后，修改显示
      type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function (newVal, oldVal, changedPath) {
        // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
        // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        console.log(newVal, oldVal, changedPath);
        if(newVal ==='show'){
          this.setData({
            modalState2:'modal-wrap2 show',
          })
        }else{
          this.setData({
            modalState2:'modal-wrap2',
          })
        }
      }
    },
    close:{ // 控制显示的，当传入的ctrl改变后，修改显示
      type: Object, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）      
    }
  },
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的初始数据
   */
  data: {
    modalState2:'modal-wrap2',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    closeModal2(){
      var myEventDetail2 = {} // detail对象，提供给事件监听函数
      var myEventOption2 = {} // 触发事件的选项
      this.triggerEvent('closeModalEvent2', myEventDetail2, myEventOption2)
    }
  }
})
