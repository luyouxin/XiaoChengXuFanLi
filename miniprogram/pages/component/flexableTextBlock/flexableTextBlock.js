
// textIn:输入支付信息，数组分段,
// fontSize: Number,
// lineHeight: Number,
// color: #121321, 

Component({

  properties: {
    textIn: Array,
    fontSize: Number,
    lineHeight: Number,
    color: String,
  },

  data: {
    _style: '',
    openFlex: false,
  },

  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached() {
      this.setStyle();
    },
    moved() { },
    detached() { },
  },

  methods: {
    setStyle(){
      this.data._style = '';
      if (this.data.fontSize && typeof (this.data.fontSize) === 'number') {
        this.data._style += 'font-size:' + this.data.fontSize + 'rpx;';
      } else {
        this.data._style += 'font-size:24rpx;';
      }
      let maxheight;
      if (this.data.lineHeight && typeof (this.data.lineHeight) === 'number') {
        this.data._style += 'line-height:' + this.data.lineHeight + 'rpx;';
        maxheight = this.data.lineHeight * 3;
      } else {
        this.data._style += 'line-height:33rpx;';
        maxheight = 99;
      }
      if (this.data.color) {
        this.data._style += 'color:' + this.data.color + ';';
      } else {
        this.data._style += 'color:#383838;';
      }
      if(!this.data.openFlex){
        this.data._style += 'height:' + maxheight + 'rpx;';
      }else{
        this.data._style += 'height:auto';
      }
      this.setData({
        _style: this.data._style,
      })
    },
    openText(){
      this.setData({
        openFlex: true,
      })
      this.setStyle();
    },
    closeText(){
      this.setData({
        openFlex: false,
      })
      this.setStyle();
    }
  }
})
