// 组件的对外属性, 是属性名到属性设置的映射表, 属性设置中可包含三个字段, type 表示属性类型, value 表示属性初始值, observer 表示属性值被更改时的响应函数
Component({
  properties:{
    tList:{
      type: Array,
      value: []
    },
    //index
    currentTab:{
      type: Number,
      value: 0,
      observer: function(newVal, oldVal){
        this.setData({
          currentTab: newVal
        })
      }
    }
  },
  // 组件的方法
  methods: {
    _swichNav: function(e){
      console.log(e);
      this.triggerEvent('changeCurrent',{
        currentNum: e.currentTarget.dataset.current
      })
    }
  }
})