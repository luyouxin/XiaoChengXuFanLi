// pages/mySetting/mySetting.js
var app = getApp();
Page({

  data: {
    modalCtrl: 'hidden',
    bool: true,
    avatarUrl: '',
    nickName: '',
    gender: 1,
    telephone: '',
    flag: false,
    codeDis: false,
    phoneCode: "获取验证码",
    telephone1: '',
    codephone: '',
    telephone1Length: '',
    codephoneLength: '',
  },

  onLoad: function(options) {
    console.log(app.globalData);
    console.log(getCurrentPages());
    let userInfo = app.globalData.userInfo;
    let userWxInfo = app.globalData.userWxInfo;
    console.log(userInfo);
    console.log(userWxInfo);
    //判断服务器是否有值
    if (userInfo.nickName == null){
      this.setData({
        nickName: userWxInfo.nickName,
      })
    }else{
      this.setData({
        nickName: userInfo.nickName,
      })
    };
    if (userInfo.gender == null){
      this.setData({
        gender: userWxInfo.gender,
      })
    }else{
      this.setData({
        gender: userInfo.gender,
      })
    };
    this.setData({
      avatarUrl: userWxInfo.avatarUrl,
      telephone: app.globalData.userInfo.mobile.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2'),
    })
  },

  handleSex() {
    let sex = app.globalData.userInfo.gender;
    wx.navigateTo({
      url: '../settingSex/settingSex?sex=' + sex,
    })
  },
  handleName() {
    wx.navigateTo({
      url: '../settingName/settingName',
    })
  },
  testModal() {
    this.setData({
      modalCtrl: 'show'
    })
  },
  onCloseEvent() {
    this.setData({
      modalCtrl: 'hidden'
    })
  },
  handleTelephone() {
    this.setData({
      bool: false,
    })
  },
  handelHide() {
    this.setData({
      bool: true,
    })
  },
  handelShow() {
    this.setData({
      bool: false,
    })
  },
  toweb: function(e) {
    const str = e.currentTarget.dataset.page;
    const time = e.currentTarget.dataset.time;
    wx.navigateTo({
      url: '../articleReading/articleReading?id=' + str + '&time=' + time,
    })
  },
  handleInput(e) {
    console.log(e);
    this.setData({
      telephone1: e.detail.value.replace(/[^\d]/g, ''),
      telephone1Length: e.detail.value.length,
    })
  },
  codeinput(e) {
    console.log(e);
    this.setData({
      codephone: e.detail.value.replace(/[^\d]/g, ''),
      codephoneLength: e.detail.value.length
    })
  },
  getMessage() {
    let telephone = this.data.telephone1;
    console.log(telephone);
    console.log(telephone.length);
    if (telephone.length != 11 || isNaN(telephone)) {
      wx.showToast({
        title: '请输入正确号码',
        icon: "loading"
      })
      setTimeout(function() {
        wx.hideToast()
      }, 2000)
      return
    }
    this.setData({
      codeDis: true
    });
    console.log(this.data.codeDis);
    // 发送短信
    wx.request({
      url: 'https://www.haitunxueche.com/student/sendMsg',
      data: {
        userPhone: telephone,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      dataType: 'json',
      success: (res) => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          wx.showToast({
            title: '短信发送成功',
            icon: "loading"
          })
          this.setData({
            phoneCode: 60,
          });
          let time = setInterval(() => {
            let phoneCode = this.data.phoneCode;
            phoneCode--;
            this.setData({
              phoneCode: phoneCode,
            });
            if (phoneCode == 0) {
              clearInterval(time);
              this.setData({
                phoneCode: "获取验证码",
                flag: true,
                codeDis: false
              });
            }
          }, 1000);
        }
      },
      fail: (err) => {
        console.log(err);
      },
      complete: (e) => {

      }
    })
  },
  handleModifyPhone() {
    let telephone = this.data.telephone1;
    if (this.data.telephone1Length != 11) {
      return;
    }
    if (this.data.codephoneLength != 6) {
      return;
    }
    wx.request({
      url: 'https://www.haitunxueche.com/student/checkMsg',
      data: {
        code: this.data.codephone,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      success: (res) => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          wx.showToast({
            title: '号码修改成功',
            icon: "loading"
          })
          let timed = setTimeout(()=> {
            this.onLoad();
            this.setData({
              bool: true,
            })
            clearTimeout(timed);
            // wx.navigateBack({
            //   delta: 2,
            // })
          }, 2100);
        }
      },
      fail: (err) => {
        console.log(err);
      },
      complete: (e) => {
        console.log(e);
      }

    })
  },
  // handlePhotograph(e) {
  //   wx.chooseImage({
  //     count: 1,
  //     sizeType: ['original', 'compressed'],
  //     sourceType: ['camera'],
  //     success: (res) => {
  //       console.log(res);
  //       const tempFilePaths = res.tempFilePaths;
  //       this.setData({
  //         avatarUrl: tempFilePaths[0],
  //       });
  //       this.getPicPromise(tempFilePaths).then(res => {
  //         console.log(res);
  //         if (res.data.code == 0 && res.data.message == "成功") {
  //           this.setData({
  //             modalCtrl: 'hidden'
  //           })
  //           wx.showToast({
  //             title: '头像修改成功',
  //             icon: "loading"
  //           })
  //           setTimeout(function () {
  //             wx.navigateBack({
  //               delta: 1,
  //             })
  //             getCurrentPages()[1].onLoad();
  //           }, 3500);
  //         }
  //       }).catch(err => {
  //         console.warn(err);
  //       })
  //     },
  //   })
  // },
  // handlePic(e) {
  //   wx.chooseImage({
  //     count: 1,
  //     sizeType: ['original', 'compressed'],
  //     sourceType: ['album'],
  //     success: (res) => {
  //       console.log(res);
  //       const tempFilePaths = res.tempFilePaths;
  //       this.setData({
  //         avatarUrl: tempFilePaths[0],
  //       })
  //       this.getPicPromise(tempFilePaths).then(res => {
  //         console.log(res);
  //         if (res.data.code == 0 && res.data.message == "成功") {
  //           this.setData({
  //             modalCtrl: 'hidden'
  //           })
  //           wx.showToast({
  //             title: '头像修改成功',
  //             icon: "loading"
  //           })
  //           setTimeout(function() {
  //             wx.navigateBack({
  //               delta: 1,
  //             })
  //             getCurrentPages()[1].onLoad();
  //           }, 3500);
  //         }
  //       }).catch(err => {
  //         console.warn(err);
  //       })
  //     },
  //   })
  // },
  // getPicPromise(_tempFilePaths) {
  //   return new Promise((resolve, reject) => {
  //     var obj = Object.assign({}, getApp().globalData.userInfo);
  //     obj.avatarUrl = _tempFilePaths;
  //     wx.request({
  //       url: 'https://www.haitunxueche.com/student/updateStudentInfo',
  //       data: obj,
  //       header: {
  //         Authorization: getApp().globalData.login.token,
  //       },
  //       method: 'POST',
  //       dataType: 'json',
  //       success: (res) => {
  //         resolve(res);
  //       },
  //       fail: (err) => {
  //         reject(err);
  //       }
  //     })
  //   })
  // }
})