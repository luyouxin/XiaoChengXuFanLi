// pages/photosShow/photosShow.js
Page({

  data: {
    currentTab: 0,
    lifePicsLength: 0,
    workPicsLength: 0,
    totalPicsLength: 0,
    lifePicsArr: [],
    workPicsArr: [],
    totalPicsArr: [],
    teacherId: null,
  },
  onLoad: function (options) {
    let log = console.log.bind(console);
    log(options);
    let flag = options.flag;
    let tid = options.tid;
    this.setData({
      teacherId: tid, 
    });
    this.onShowTypePic(flag);
    this.getPicsPromise(tid, flag).then(res=>{
      log(res);
      if (res.data.code == 0 && res.data.message == "成功"){
        let lifePicsLength = res.data.data.lifePics.length;
        let workPicsLength = res.data.data.workPics.length;
        let totalPicsLength = lifePicsLength + workPicsLength;
        let lifePicsArr = res.data.data.lifePics;
        let workPicsArr = res.data.data.workPics;
        let totalPicsArr = [...lifePicsArr, ...workPicsArr];
        log(totalPicsArr);
        log(lifePicsLength, workPicsLength, totalPicsLength);
        this.setData({
          totalPicsLength: totalPicsLength,
          lifePicsLength: lifePicsLength,
          workPicsLength: workPicsLength,
          totalPicsArr: totalPicsArr,
          lifePicsArr: lifePicsArr,
          workPicsArr: workPicsArr,
        })
      }else{
        console.error(res);
      }
    }).catch(reason=>{  
      console.log(reason);
    })
  },
  swiperTab(e){
    console.log(e);
    this.setData({
      currentTab: e.detail.current,
    })
  },
  clickTab(e){
    console.log(e);
    if(this.data.currentTab === e.target.dataset.current){
      return false;
    }else{
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
  },
  onShowTypePic(params){
    if (params == 'lifePics'){
      this.setData({
        currentTab: 2,
      })
    } else if (params == 'workPics'){
      this.setData({
        currentTab: 1,
      })
    }else{
      this.setData({
        currentTab: 0,
      })
    }
  },
  getPicsPromise(_tid,_flag){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getTeacherPicVo',
        data: {
          id: _tid,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }
      })
    })
  },
  // previewImage(e){
  //   console.log(e);
  //   let current = e.currentTarget.dataset.src;
  //   let ArrImg = ["https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201811/1065964301835415552.jpg", "https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201812/1072517185471025152.jpg", "https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201812/1072517084266663936.jpg", "https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201811/1065847140788039680.jpg", "https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201812/1072517228085153792.jpg", "https://htxc.oss-cn-shenzhen.aliyuncs.com/userfiles/fileupload/201812/1072517242811355136.jpg"];
  //   let baseUrl = 'https://htxc.oss-cn-shenzhen.aliyuncs.com';
  //   wx.previewImage({
  //     current: baseUrl+current,
  //     urls: ArrImg,
  //   })
  // }

})