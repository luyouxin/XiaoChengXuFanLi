// pages/exam/component/examCtrl.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    subject:Number,
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    doExercise(e){
      this.triggerEvent('doExercise',{ type: e.currentTarget.dataset.type, subject: this.data.subject },{});
    },
    mockExam(){
      wx.navigateTo({
        url: './mockExam/doExam?subject=' + this.data.subject,
      })
    },
    statics(e){
      const key = e.currentTarget.dataset.key;
      wx.navigateTo({
        url: './statistics/statistics?key=' + key + '&subject=' + this.data.subject,
      })
    },
    toLight(){
      wx.navigateTo({
        url: './exercise/lightAndVoice',
      })
    },
    toAllExec(){
      this.triggerEvent('doExerciseAll', {}, {})
    }
  }
})
