// pages/exam/component/bottomPanel.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    total: Number,
    alreadyDo: Number,
    currentTimu: Number,   
    userReponse:Object, 
    tableOfContents: Array,
    mode: Number,
  },

  /**
   * 组件的初始数据
   */
  data: {
    panelStyle: 'height:88rpx;',
    state: 'close',
    correct: 0,
    wrong: 0,
  },

  methods: {
    rpxToPx: function (num) {
      const sysInfo = wx.getSystemInfoSync();
      return num / 750 * sysInfo.windowWidth;
    },
    openPanel() { // 打开底部面板
      if (this.data.state === 'close') {
        let leAnimation = wx.createAnimation({
          duration: 220,
          timingFunction: 'ease',
          delay: 50,
        });
        leAnimation.height(this.rpxToPx(655)).step();
        const sInfo = wx.getSystemInfoSync();
        this.setData({
          panelStyle: 'height:' + sInfo.screenHeight + 'px;',
          state: 'open',
          panelOpenAnimate: leAnimation.export(),
        })
      } else {
        let leAnimation = wx.createAnimation({
          duration: 220,
          timingFunction: 'ease',
          delay: 50,
        });
        leAnimation.height(0).step();
        this.setData({
          panelStyle: 'height:88rpx;',
          state: 'close',
          panelOpenAnimate: leAnimation.export(),
        })
      }
    },
    answerCount(d){
      if(d.result === 'wrong'){
        this.setData({
          wrong: this.data.wrong + 1,
        });
      }else{
        this.setData({
          correct: this.data.correct + 1,
        });
      }
    },
    toTimu(e){
      // console.log(e);
      let index = e.currentTarget.dataset.index; // 题目的id
      this.openPanel();
      this.triggerEvent('jumpToTimu',{index:index},{});
    },
    jiaojuan(){
      this.triggerEvent('finishExam',{},{});
    },
  }
})