// pages/exam/component/questionBody.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    question: {
      type: Number,
      value: '',
      observer: function (n, o, p) {
        if (n === 0) {
          // this.setContent();
        }
      }
    },
    mode: Number,
    content: {
      type: Object,
      value: '',
      observer: function (n, o, p) {
        console.log('income content', this.data);
        // this.showAnswer();
      }
    },
    userReponse: {
      type: Object,
      observer: function (n, o, p) {
        console.log('in userResponse', n)
      }
    }
  },

  data: {
    answer: 'noResponse',
    pageData: [],
    pageCurrent: 0,
    // duration: 300,
    circular: true,
  },

  methods: {
    openBig(e) {
      const src = e.currentTarget.dataset.src;
      wx.previewImage({
        current: src,
        urls: [src],
      });
    },
    youNeedContent() {
      this.triggerEvent('refreshContent', {
        currentTimuIndex: 0,
        currentSwipeItemIndex: 0,
      }, {});
    },
    changePage(e) { // 切换页面
      if (e.detail.source !== 'touch') {
        return;
      }
      const curIndex = e.detail.current;  // 当前浏览的页面在3个幅面里的序号
      const direct = this.getPageChangeDirection(curIndex);
      console.log(direct);
      // this.setData({ pageCurrent: curIndex });
      const currentPage = this.data.content[curIndex];

      this.triggerEvent('refreshContent', {
        currentTimuIndex: currentPage.index,
        currentSwipeItemIndex: curIndex,
      }, {});
    },
    getPageChangeDirection(curIndex) { // 判断左右翻页用
      let test = curIndex - this.data.pageCurrent;
      if (test === 1 || test === -2) {
        return 'right';
      } else {
        return 'left';
      }
    },
    numberToAlaphabet(num) {
      let _num = Number(num);
      switch (_num) {
        case 1:
          return 'A';
        case 2:
          return 'B';
        case 3:
          return 'C';
        case 4:
          return 'D';
        case 5:
          return 'E';
      }
    },
    answerQ(e) {
      this.triggerEvent('response', {
        response: e.detail.tag,
        timuId: e.detail.id,
      }, {});
    },
    jumpTimu(num) {
      console.log(num);
      let _index = num.detail.index;
      wx.showLoading({
        title: '',
        mask: true,
      })
      const curIndex = this.data.pageCurrent;  // 当前浏览的页面在3个幅面里的序号
      // console.log(curIndex)
      const long = this.data.userReponse.length;
      const currentPage = this.data.content[curIndex];
      currentPage.index = _index;
      if (long > 3) { // 题目必须大于3题才出发无线循环,返回的是序号，由1开始
        let arr = [-1, -1, -1];
        arr[curIndex] = currentPage.index;
        let left, right;
        switch (curIndex) {
          case 0:
            left = 2;
            right = 1;
            break;
          case 1:
            left = 0;
            right = 2;
            break;
          case 2:
            left = 1;
            right = 0;
            break;
        }
        arr[left] = (currentPage.index - 1) > 0 ? currentPage.index - 1 : long - 1;
        arr[right] = (currentPage.index + 1) > (long - 1) ? 1 : currentPage.index + 1;
        this.triggerEvent('refreshContent', {
          pageNum: arr,
          currentIndex: currentPage.index
        }, {});
      };
      wx.hideLoading();
    }
  }
})