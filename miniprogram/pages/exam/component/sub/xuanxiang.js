Component({
  properties: {
    mode: {
      type: Number,
      observer(n, o, p) {
        this.modeChange(n);
      }
    },
    userReponse: {
      type: Object,
      observer(n, o, p) {
        if(n && !o){
          this.refreshUserResponse();
          return;
        }
        if(n && o){
          let keyNew = Object.keys(n);
          let keyOld = Object.keys(o);
          if(keyNew.length !== keyOld.length){
            this.refreshUserResponse();
          }
        }
      }
    },
    item: {
      type: Object,
      observer(n, o, p) {
        if (n) {
          // console.log(n)
          this.makeStruct();
        }
      }
    },
  },

  data: {
    list: [], // 所有选项的载体
    hasUserAnswerMe: false, // 标记用户是否回答本体或者学习模式
  },
  lifetimes: {
    attached: function () {
      console.log('选项', this.data);
    },
    detached: function () {
    },
  },
  methods: {
    makeStruct() { // 初始化数据的构造
      let list = [].concat(this.data.item.answerObj);
      for (let i of list) {
        i.choose = false;
      }
      this.setData({
        list: list,
        id: this.data.item.id,
        multi: this.data.item.multi,
      })
      this.refreshUserResponse();
    },
    userChangeResponse() {
      if (this.data.id && this.data.userReponse[this.data.id]) {
        this.setData({ hasUserAnswerMe: true, })
      } else {
        this.setData({ hasUserAnswerMe: false, })
      }
      this.refreshUserResponse();
    },
    modeChange(n) {
      if (this.data.id) {
        if (n == 1) {
          this.setData({ hasUserAnswerMe: false, });
        }
        this.refreshUserResponse(); 
      }
    },
    tagToNumber(tag) {
      let tags = {
        A: 0,
        B: 1,
        C: 2,
        D: 3,
        E: 4,
      };
      return tags[tag] >= 0 ? tags[tag] : null;
    },
    refreshUserResponse() {
      if (this.data.mode == 0 || this.data.mode == 3) {
        if (this.data.userReponse[this.data.id]) {
          let answerList = this.data.userReponse[this.data.id];
          this.setData({ hasUserAnswerMe : true, });
          for (let i of answerList) {
            let index = this.tagToNumber(i);
            let item = this.data.list[index];
            item.showImg = item.rightAnswer ? 'correct' : 'wrong';
          };
          for (let i of this.data.list) {
            if (i.rightAnswer) {
              i.showImg = 'correct';
            }
          }
        }else{
          this.setData({ hasUserAnswerMe: false, });
        }
        this.setData({
          list: this.data.list,
        })
      }
    },
    tapChoose(e) {
      if (this.data.mode === 1) { return; }
      if (this.data.userReponse[this.data.id]) { // 已做答，不可以修改
        return;
      }
      let target = e.currentTarget.dataset.me;
      if (this.data.multi) { // 多选
        for (let i of this.data.list) {
          if (i.tag === target.tag) {
            i.choose = true;         
            break;
          }
        };
        this.setData({ list: this.data.list, });
      } else { // 单选
        this.setData({ hasUserAnswerMe: true, });
        let item;
        for (let i of this.data.list) {
          if (i.tag === target.tag) {
            item = i;
            break;
          }
        };
        this.triggerEvent('userRes', {
          id: this.data.id,
          tag: item.rightAnswer ? [] : [item.tag],
        }, {});
      }
    },
    multiConfirm(){
      let list = [];
      for(let i of this.data.list){
        if(i.choose){
          list.push(i.tag);
        }
      }
      console.log(list);
      if(list.length < 1){
        return;
      }
      this.triggerEvent('userRes', {
        id: this.data.id,
        tag: list,
      }, {});
    },
  }
})
