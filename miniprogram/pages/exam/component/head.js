// pages/exam/component/head.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    blockStep:0,
    animation:{},
  },
  attached(){
    const animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease-out',
    });
    this.ami = animation;
  },

  /**
   * 组件的方法列表
   */
  methods: {
    rpxToPx: function (num) {
      const sysInfo = wx.getSystemInfoSync();
      return num / 750 * sysInfo.windowWidth;
    },
    kemu(e){
      const index = Number(e.currentTarget.dataset.sbj);
      if(index === this.data.blockStep){ return; }
      const distance = this.rpxToPx(194 * index);
      // console.log(distance);
      this.ami.translateX( distance ).step();
      this.setData({
        blockStep:index,
        animation: this.ami.export(),
      })
      this.triggerEvent('subjectState',{index:index},{});
    }
  }
})
