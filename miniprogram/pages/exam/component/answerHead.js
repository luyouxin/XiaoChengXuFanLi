// pages/exam/component/answerHead.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    startIndex:{
      type: Number,
      value: "",
      observer(n,o,p){
        // console.log(n)
        this.setData({
          mode: n,
        })
      }
    },
    names:{
      type:String,
      value: '',
      observer(n,o,p){
        let tmp = n.split(',');
        this.setData({
          btnName:tmp,
        });
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeMode(e){
      this.triggerEvent('changeMode',{ index: e.currentTarget.dataset.index },{})
    }
  }
})
