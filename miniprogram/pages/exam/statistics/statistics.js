// pages/exam/statistics/statistics.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    names: ['练习统计,考试记录'],
    startIndex: 0,
    isEmpty: null,
    examResult: [],
    execResult: [],
  },

  onLoad: function (options) {
    // console.log(options);
    if (options.key === 'exec') {
      this.setData({
        startIndex: 0,
        subject: options.subject,
      })
    } else if (options.key === 'other') {
      this.setData({
        startIndex: 1,
        subject: options.subject,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadExam();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  loadExam() {
    wx.request({
      url: getApp().globalData.serverAddress + 'questions/listExam',
      data: {
        subject: 1,
        type: 'C1',
      },
      header: {
        Authorization: getApp().globalData.login.token
      },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (res) => {
        if (res.data.code === 0) {
          if (res.data.data.length < 1) {
            this.setData({
              examResult: [],
            })
          }else{
            this.setData({
              examResult: res.data.data,
            })
          }
          this.monteData();
        }
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  monteData() {
    if (this.data.startIndex === 1) {
      if (this.data.examResult.length < 1) {
        this.setData({ isEmpty: true, });
      } else {
        let dat = this.rawExamDataConvert();
        this.setData({
          isEmpty: false,
          content: dat,
        });
      }
    }
  },
  rawExamDataConvert(){
    let resultForReturn = [];
    for (let i of this.data.examResult){
      if(!i.timeString){
        let timeString = i.useTime ? i.useTime : 0;
        timeString = Math.ceil(timeString / 60 ) + '分' + Math.ceil(timeString % 60) + '秒';
        i.timeString = timeString;
      }
      resultForReturn.push(i);
    }
    return resultForReturn;
  },
  startNewExam(){
    wx.redirectTo({
      url: '../mockExam/doExam?subject=' + this.data.subject,
    })
  },
})