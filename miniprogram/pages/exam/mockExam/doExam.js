// pages/exam/mockExam/doExam.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      userPhoto: getApp().globalData.userInfo.avatarUrl || null,
      userName: getApp().globalData.userInfo.nickName || null,
      subject: options.subject === '0' ? '1': '4',
      subjectName: options.subject === '0' ? '科目一': '科目四',
      total: options.subject === '0' ? '100': '50',
      emaxDuration: '45',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  startExam(){
    const failToast = (e) => {
      wx.showToast({
        title: '创建考试失败 ' + e,
        icon: 'none',
        duration: 1700,
        mask: true,
      })
    }
    // 创建考试
    wx.showLoading({
      title: '',
      mask: true,
    })
    wx.request({
      url: getApp().globalData.serverAddress + 'questions/getExam',
      data: {
        subject: this.data.subject,
        type: 'C1'
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: function(res) {
        if(res.data.code === 0){
          wx.setStorageSync('newExam', res.data.data);
          wx.redirectTo({
            url: '/pages/exam/exercise/examQuestion',
          })
        }else{
          failToast(JSON.stringify(res.data));
        }
      },
      fail: function(res) {
        failToast(JSON.stringify(res));
      },
      complete: function(res) {
        wx.hideLoading();
      },
    })
  }
})