const lQ = require('./func/loadQuestion.js');
const refreshContent = require('./func/showTimu.js').refreshContent;
// const uploadAnPractice = require('./func/uploadExciserResult.js').uploadAnPractice;

const examInit = require('./func/loadExam.js');

Page({

  data: {
    startIndex: 0,
    content: [],
    currentTimu: 1,
    timuPool: [],
    userReponse: {},
    examDuration: 44 * 60 * 1000, // 考试时间
    examBegin: null,
    examStart: null,
    examEnd: null,
    examRestTime: '45:00',
    timeInterval: null,
  },

  onLoad: function (options) {
    examInit(this);
  },

  onReady: function () {

  },

  onShow: function () {

  },

  onHide: function () {

  },

  onUnload: function () {
  },

  startExamTime() {
    let time = Date.now();
    this.setData({
      examBegin: time,
      examStart: time,
      examEnd: time + this.data.examDuration,
    });
    setTimeout(() => {
      this.changeTimeIntervale()
    }, 1000);
  },
  changeTimeIntervale() {
    let rest = this.data.examStart + 1000;
    rest = this.data.examEnd - rest;
    if (rest > 0) {
      rest = Math.ceil(rest / 1000);
      let min = Math.ceil(rest / 60);
      min  = min > 9 ? min : '0' + min;
      let sec = Math.ceil(rest % 60);
      sec  = sec > 9 ? sec : '0' + sec;
      let str = min + ':' + sec;
      this.setData({
        examRestTime: str,
        examStart: this.data.examStart + 1000,
      });
    } else {
      this.setData({
        examRestTime: '考试结束',
      });
      this.finishExam();
    }
    setTimeout(() => {
      if(this.data.examRestTime !== '考试结束'){
        this.changeTimeIntervale();
      }
    }, 1000);
  },
  showQuestion(questionIndex) {
    // 恢复到上次考题在这里，未做
    this.setData({
      question: questionIndex,
    })
  },
  changeMode(e) {
    this.setData({
      question: Number(e.detail.index),
      startIndex: Number(e.detail.index),
    })
  },
  refreshContent(d) {
    refreshContent(this, d.detail.currentTimuIndex, d.detail.currentSwipeItemIndex);
    this.setData({ currentTimu: d.detail.currentTimuIndex + 1, });
  },
  response(e) {
    console.log(e);
    const id = e.detail.timuId;
    const resp = e.detail.response; // 空数组代表回答正确，反之为错误的
    this.data.userReponse[id] = resp;
    this.setData({
      userReponse: this.data.userReponse,
    })
    let bottomData = {
      result: resp.length > 0 ? 'wrong' : 'right',
    }
    const bp = this.selectComponent('#bp');
    bp.answerCount(bottomData);
    // uploadAnPractice(id, resp.length > 0 ? 'wrong' : 'right');
  },
  jumpToTimu(e) { // 题目跳转用
    refreshContent(this, e.detail.index + 1, 1);
    this.setData({ currentTimu: e.detail.index + 1, });
  },
  finishExam(){
    console.log('finish exam');
    let userTime,wrongIds = [];
    // 错误的概念，没写和做错
    for (let i of this.data.tableOfContents){
      let t = this.data.userReponse[i];
      if(t){ // 作答
        if(t.length > 0){
          wrongIds.push(i);
        }
      }else{ // 没作答
        wrongIds.push(i);
      }
    }
    wrongIds = wrongIds.toString();
    /// 考试时间用秒
    userTime = Math.ceil( (this.data.examStart - this.data.examBegin) / 1000);
    wx.showLoading({
      title: '',
      mask: true,
    })
    wx.request({
      url: getApp().globalData.serverAddress + 'questions/saveExam',
      data: {
        id: this.data.examId,
        userTime:userTime,
        wrongIds:wrongIds,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if(res.data.code === 0){
          wx.showModal({
            title: '考试结束',
            content: '成绩已经提交',
            showCancel: false,
            confirmText: '关闭',
            confirmColor: '',
            success: function(res) {
              wx.navigateBack({
                delta: 1,
              })
            },
            fail: function(res) {},
            complete: function(res) {},
          })
        }else{
          console.warn(res.data);
        }
      },
      fail: function(res) {
        console.warn(res);
      },
      complete: function(res) {
        wx.hideLoading();
      },
    })
  },
})