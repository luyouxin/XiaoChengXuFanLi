// pages/exam/exercise/chooseCharpter.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chapeter:null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let test = options.key;
    let chapeterData = wx.getStorageSync('examVarName').data;
    for(let i of chapeterData){
      if (i.name === test){
        this.setData({
          chapeter: i.data,
          varName: test,
        })
        break;
      }
      if (i.name === test) {
        this.setData({
          chapeter: i.data,
          varName: test,
        })
        break;
      }
    };
    this.setData({ subject: options.subject, });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  answerIt:function(e){
    const val = e.currentTarget.dataset.val;
    console.log(this.data);
    // return;
    wx.navigateTo({
      url: './answerQuestion?' + this.data.varName + '=' + val + '&subject=' + this.data.subject,
    })
  }, 
})