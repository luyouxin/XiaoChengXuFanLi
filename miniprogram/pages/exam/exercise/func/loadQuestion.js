module.exports = function(options,_this,page=1,size=3000){ // 读取题目结构
  wx.showLoading({
    title: '读取练习题',
    mask: true,
  })
  let subject = options.subject.toString() === '3' ? 4 : 1;
  let params = {
    subject:subject,
    page:page,
    size:size,
    // questionType: '3'
  };
  console.log(options);
  if (options.chapterId){
    params.chapterId = options.chapterId;
  }
  if(options.supperType){
    params.supperType = options.supperType;
  }
  if (!getApp().globalData.login || !getApp().globalData.login.token){
    wx.showToast({
      title: 'token无效',
      icon: 'none',
      duration: 2000,
      mask: true,
    })
  }
  wx.request({
    url: getApp().globalData.serverAddress + 'questions/list',
    data: params,
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    dataType: 'json',
    responseType: 'text',
    success: function(res) {
      if(res.data.code === 0){
        if(res.data.data.records.length > 0){
          let arr = res.data.data.records;
          loadFinish(arr,_this);
        }else{
          wx.showToast({
            title: '对应分类没有题目',
            icon: 'none',
            duration: 2000,
            mask: true,
          })
          setTimeout(function(){
            wx.navigateBack({
              delta: 1,
            })
          },2100);
        }
      }else{

      }
    },
    fail: function(res) {
      wx.hideLoading();
    },
    complete: function(res) {
      wx.hideLoading()
    },
  })
}

// const handleTimu = require('handleTimu.js');

const loadFinish = function (dat,_this) {
  // handleTimu(dat,_this);
  _this.setData({ 
    total: dat.length,
    tableOfContents : dat,
    });  
  const qb = _this.selectComponent('#qb');
  const result = qb.youNeedContent();
}