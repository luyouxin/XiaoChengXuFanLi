function requestQuestion(_this, ids) {
  wx.request({
    url: getApp().globalData.serverAddress + 'questions/get',
    data: {
      ids: ids,
    },
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    success: function (res) {
      if (res.data.code === 0) {
        let t = [];
        let _ids = ids.split(',');
        for (let i of _ids) { // 纠正顺序
          for (let j of res.data.data) {
            if (i === j.id) {
              t.push(j);
              break;
            }
          }
        }
        for (let i of t) {
          let tmp = {};
          tmp.explain = i.explain;
          tmp.difficulty = i.difficulty;
          tmp.id = i.id;
          tmp.question = i.question;
          let _answer = i.answer.split(',');
          tmp.answerObj = [];
          for (let j = 65; j <= 69; j++) {
            let code = String.fromCharCode(j);
            let varName = 'option' + code;
            if (i[varName] && i[varName].length > 0) {
              let obj = {
                text: i[varName],
                rightAnswer: false, // 标记是否正确答案
                tag: code,
              }
              tmp.answerObj.push(obj);
            }
          }
          for (let j of _answer) {
            j = Number(j) - 1;
            if(!tmp.answerObj[j]){
              console.warn(tmp.answerObj,j,tmp.id);
              continue;
            }
            tmp.answerObj[j].rightAnswer = true;
          }
          if (_answer.length <= 1) {
            tmp.multi = false;
          } else {
            tmp.multi = true;
          }
          if (i.pic && i.pic.length > 2) {
            tmp.pic = i.pic;
          }
          if (i.video && i.video.length > 2) {
            tmp.video = i.video;
          }

          tmp.index = _this.data.tableOfContents.indexOf(tmp.id);

          if (_this.data.timuPool.length <= 50) {
            _this.data.timuPool.push(tmp);
          } else {
            _this.data.timuPool.shift();
            _this.data.timuPool.push(tmp);
          }
        }

        monteContent(_this);
      }
    },
    fail: function (res) { },
    complete: function (res) { },
  })
}

function monteContent(_this) { // 数据的装载方法
  // 由题目池子中读出对应的题目数据，替换content的内容,content里的id数据已经是最新的了
  for (let i = 0; i < _this.data.content.length; i++) {
    if (typeof (_this.data.content[i]) !== 'string') {
      continue;
    }
    for (let j of _this.data.timuPool) {
      if (j.id == _this.data.content[i]) {
        _this.data.content[i] = j;
        break;
      }
    }
  }
  _this.setData({
    content: _this.data.content,
  });
}

module.exports.refreshContent = indexForSwipeItem;

function indexForSwipeItem(_this, currentTimuIndex, currentSwipeItemIndex) { // 改变和修改页面内容
  // console.log(currentTimuIndex, currentSwipeItemIndex); // 当前页面题目序号、当前页面序号，由0开始
  // content的内容，回答的状态
  let pageStruct = [currentTimuIndex - 2, currentTimuIndex - 1, currentTimuIndex, currentTimuIndex + 1, currentTimuIndex + 2];
  for (let i = 0; i < pageStruct.length; i++) {
    if (pageStruct[i] === -2) {
      pageStruct[i] = _this.data.tableOfContents.length - 2;
    } else if (pageStruct[i] === -1) {
      pageStruct[i] = _this.data.tableOfContents.length - 1;
    } else if (pageStruct[i] === _this.data.tableOfContents.length - 1) {
      // pageStruct[i] = 0;
    } else if (pageStruct[i] === _this.data.tableOfContents.length) {
      pageStruct[i] = 0;
    } else if (pageStruct[i] === _this.data.tableOfContents.length + 1) {
      pageStruct[i] = 1;
    }
  }
  let tmp = [-1, -1, -1, -1, -1];
  let loop = 5, begin = 2, run = currentSwipeItemIndex;

  while (loop > 0) {
    if (pageStruct[begin] === undefined) {
      begin = 0;
    }
    if (!tmp[run]) {
      run = 0;
    }
    tmp[run] = pageStruct[begin];
    begin++; run++; loop--;
  }

  console.log(pageStruct, tmp);
  makeIdsArray(tmp, _this);
}

function makeIdsArray(structData, _this) { // 由序号转为题目的id数组
  let ids = [];
  for (let i of structData) {
    ids.push(_this.data.tableOfContents[i]);
  }
  // console.log(ids);
  checkContentCorrespons(_this, ids);
}

function checkContentCorrespons(_this, data) { // 检测当前的页面内容类是否符合题目id数组
  let idNotInContent = [];
  for (let i = 0; i < data.length; i++) {
    if (_this.data.content[i] && _this.data.content[i].id != data[i]) {
      _this.data.content[i] = data[i];
    } else if (!_this.data.content[i]) {
      _this.data.content[i] = data[i];
    }
  }
  console.log('初步content数据', _this.data.content);
  // content的内容最终有2种，为objcet就不需要更新，如果是string就需要更新数据
  monteData(_this, idNotInContent);
}

function monteData(_this, newIdsArray) {  // 读取池子中的题目缓存数据，并更改swipeItem的内容
  let needRequestTimu = [];
  for (let i of _this.data.content) {
    if (typeof (i) === 'string') { // 需要绑定，为object代表不需要更新
      let dataAlradyBuffer = false;
      for (let j of _this.data.timuPool) {
        if (j.id == i) {
          dataAlradyBuffer = true;
          i = j;
          break;
        }
      }
      if (dataAlradyBuffer) { // 数据已经缓冲过
        _this.setData({
          content: _this.data.content,
        })
      } else { // 需要由网络获取数据
        needRequestTimu.push(i);
      }
    }
  }
  let ids = needRequestTimu.toString();
  if (ids) {
    requestQuestion(_this, ids);
  };
  monteContent(_this);
}

