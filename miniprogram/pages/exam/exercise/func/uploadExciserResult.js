module.exports.uploadAnPractice = function(id,result){
  let t;
  if(result === 'right'){
    t = 'questions/incRight';
  }else{
    t = 'questions/incError';
  }
  wx.request({
    url: getApp().globalData.serverAddress + t,
    data: {
      ids: id,
    },
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    success: function(res) {},
    fail: function(res) {},
    complete: function(res) {},
  })
}