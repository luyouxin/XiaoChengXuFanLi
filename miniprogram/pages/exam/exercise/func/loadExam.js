module.exports = function(_this,){
  let exam = wx.getStorageSync('newExam');
  console.log(exam);
  _this.setData({
    examId: exam.id,
  })
  let timuPool = exam.questionIds.split(',');
  loadFinish(timuPool,_this);
}

const loadFinish = function (dat, _this) {
  _this.setData({
    total: dat.length,
    tableOfContents: dat,
  });
  const qb = _this.selectComponent('#qb');
  const result = qb.youNeedContent();
  _this.startExamTime();
}