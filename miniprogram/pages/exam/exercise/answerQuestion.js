const lQ = require('./func/loadQuestion.js');
// const handleTimu = require('./func/handleTimu.js');
// const monteContent = require('./func/showTimu.js').monteContent;
const refreshContent = require('./func/showTimu.js').refreshContent;
const uploadAnPractice = require('./func/uploadExciserResult.js').uploadAnPractice;

Page({

  data: {
    startIndex: 0,
    names: ['练习模式', '学习模式'],
    content: [],
    currentTimu: 1,
    timuPool: [],
    userReponse: {},
  },

  onLoad: function (options) {
    lQ(options, this);
  },

  onReady: function () {

  },

  onShow: function () {

  },

  onHide: function () {

  },

  onUnload: function () {
    this.setData({});
  },

  onShareAppMessage: function () {

  },
  showQuestion(questionIndex) {
    // 恢复到上次考题在这里，未做
    this.setData({
      question: questionIndex,
    })
  },
  changeMode(e) {
    this.setData({
      question: Number(e.detail.index),
      startIndex: Number(e.detail.index),
    })
  },
  refreshContent(d) {
    refreshContent(this, d.detail.currentTimuIndex, d.detail.currentSwipeItemIndex);
    this.setData({ currentTimu: d.detail.currentTimuIndex + 1,});
  },
  response(e) {
    // console.log(e);
    const id = e.detail.timuId;
    const resp = e.detail.response; // 空数组代表回答正确，反之为错误的
    this.data.userReponse[id] = resp;
    this.setData({
      userReponse: this.data.userReponse,
    })
    let bottomData = {
      result: resp.length > 0 ? 'wrong' : 'right',
    }
    const bp = this.selectComponent('#bp');
    bp.answerCount(bottomData);
    uploadAnPractice(id, resp.length > 0 ? 'wrong' : 'right');
  },
  jumpToTimu(e) { // 题目跳转用
    refreshContent(this, e.detail.index + 1, 1);
    this.setData({ currentTimu: e.detail.index + 1, });
  },
})