const initDic = require('./func/initDictionary.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    subject:0,
    storageExamOptionVarName :'examVarName',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    initDic();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  subjectState(e){
    console.log(e);
    this.setData({
      subject: e.detail.index,
    })
  },
  toLib(){
    wx.navigateTo({
      url: 'chooseLib',
    })
  },
  doExercise(e){
    // console.log(e);
    let testV = wx.getStorageSync(this.data.storageExamOptionVarName);
    let test;
    if(testV){
      for(let i of testV.data){
        if (i.name === e.detail.type){
          test = i.name;
          break;
        }
      }
    }
    if(!test){
      console.warn('没有获得对应信息! ',testV);
      return;
    }
    wx.navigateTo({
      url: './exercise/chooseCharpter?key=' + test + '&subject=' + e.detail.subject,
    })
  },
  doExerciseAll(){
    wx.navigateTo({
      url: './exercise/answerQuestion' + '?subject=' + this.data.subject,
    })
  }
})