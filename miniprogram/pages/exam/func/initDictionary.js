module.exports = function () {
  let storageExamOptionVarName = 'examVarName';
  let finalDictionary = {
    sucess:0,
    fail: 0,
    data: [],
  };
  const dicFunc = (name) => {
    wx.request({
      url: getApp().globalData.serverAddress + 'common/getDict',
      data: {
        name: name.name,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: function (res) { 
        if(res.data.code === 0){
          finalDictionary.sucess += 1;
          let tmp = {
            name: name.v_name,
            data: res.data.data,
          }
          finalDictionary.data.push(tmp);
        }else{
          finalDictionary.fail += 1;
        }
      },
      fail: function (res) { 
        finalDictionary.fail += 1;
      },
      complete: function (res) { 
        // console.log(finalDictionary);
        wx.setStorageSync(storageExamOptionVarName, finalDictionary);
      },
    })
  }
  const list = [
    { name: 'car_typ', v_name: 'carType' },
    { name: 'question_type', v_name: 'chapterId' }, // 章节
    { name: 'supperType', v_name: 'supperType' },
    { name: 'school_course_type', v_name: 'type' },
    { name: 'school_subject', v_name: 'subject' },
    { name: 'questionType', v_name: 'questionType' },
  ];
  for (let i of list){
    dicFunc(i);
  }
}