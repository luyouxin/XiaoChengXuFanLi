const toStudentReq = require('../../utils/studentCtrl.js').ToStudentReq;
const reqStudent = require('../../utils/studentCtrl.js').ReqStudent;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    testTeacherOpenId: "oUOxG47yXcyxknE2YpVxxwAyYLJc", // 测试用教师oid
    teacher: null,
    lisenceType: null,
    phoneNumber: '',
    name: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      tid: getApp().globalData.offlineTeacher,
    }, () => {
      getApp().globalData.offlineTeacher = null;
    })
    const waitUntilLogin = setTimeout( ()=>{
      if (!getApp().globalData.login || !getApp().globalData.login.token){
        return waitUntilLogin();
      }else{
        return this.requestTeacher();        
      }
    },300)
  },

  onShow: function () {

  },

  requestTeacher() {
    // let id = this.data.testTeacherOpenId;
    let id = this.data.tid;
    if(!id){
      wx.showModal({
        content: '教师id出错',
        showCancel: false,
        confirmText: '返回',
        confirmColor: '#fe2742',
        success: function(res) {
          wx.navigateBack();
        },
      })
      return;
    }
    toStudentReq(
      id,
      (res) => {
        console.log(res);
        this.setData({
          teacher: res,
        })
      },
      (err) => {
        console.log(err);
      }
    )
  },
  chooseType: function () {
    if (!this.data.teacher) { return; }
    let tmp = [];
    for (let i of this.data.teacher.classList) {
      tmp.push(i.name);
    }
    if(tmp.length > 6){
      tmp = tmp.slice(0,6);
    }
    wx.showActionSheet({
      itemList: tmp,
      itemColor: '#383838',
      success: (res) => {
        for (let i of this.data.teacher.classList) {
          if (tmp[res.tapIndex] === i.name) {
            this.setData({
              lisenceType: i.id,
              showType: i.name,
            })
            break;
          }
        }
        this.validate();
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  chooseSubject() {
    wx.showActionSheet({
      itemList: ['科二', '科三'],
      itemColor: '',
      success: (res) => {
        if (res.tapIndex === 0) {
          this.setData({
            // subject: 2,
            showSubject: '科二',
          })
        } else if (res.tapIndex === 1) {
          this.setData({
            // subject: 3,
            showSubject: '科三',
          })
        }
        this.validate();
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  changeNumber(e) {
    this.setData({
      phoneNumber: e.detail.value.trim(),
    }, () => {
      this.validate();
    })
  },
  changeName(e) {
    this.setData({
      name: e.detail.value.trim(),
    }, () => {
      this.validate();
    })
  },
  validate() {
    if (this.data.lisenceType && this.data.showSubject && this.data.name.length >= 2) {
      this.setData({
        validate: true,
      })
    } else {
      this.setData({
        validate: false,
      })
    }
  },
  submit() {
    if (!this.data.validate && this.data.teacher) {
      wx.showModal({
        content: '输入信息不完备',
        showCancel: false,
        confirmText: '关闭',
        confirmColor: '',
        success: function(res) {}, 
      })
      return;
    }
    let data = {
      classId: this.data.lisenceType,
      subject: this.data.showSubject==='科二' ? 2 : 3,
      realName:this.data.name,
      // 漏电话号码
      teacherId: this.data.teacher.teacherId,
    }
    reqStudent(
      data,
      (res)=>{
        console.log(res);
        wx.showModal({
          title: '申请成功',
          content: '线下学员申请已经发送，请等待教练审核，审核通过后，重新打开小程序',
          showCancel: false,
          confirmText: '知道了',
          confirmColor: '',
          success: function(res) {
            wx.reLaunch({
              url: '../index/index',  
            })
          },
        })
      },
      (err)=>{
        console.warn(err);
        wx.showModal({
          content: JSON.stringify(err),
          showCancel: false,          
          confirmText: '关闭',
          confirmColor: '',
          success: function(res) {},
        })
      }
    )
  },
})