
// const tjdata = require('../../lib/tijiandata.js');
const cityBL = require('../../utils/cityByLocation.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      mapKey: 'HTQBZ-FN3WI-IIRG7-5JWI4-74OFZ-AYFFJ',
      working: '营业时间 09:00-17:00'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // console.log('体检站数据', tjdata);
    this.getListVo();
    // this.setData({
    //   tjz: tjdata,
    // })
    this.setData({
      map: this.selectComponent('#customMap')
    })
  },

  onShow: function () {
   
  },

  onHide: function () {

  },

  onUnload: function () {

  },

  onShareAppMessage: function () {

  },
  // 获取体检站数据
  getListVo(){
    wx.request({
      url: getApp().globalData.serverAddress + 'common/listStoreVo',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if(res.data.code === 0){
          // console.log(res.data.data);
          let list = [];
          for(let i of res.data.data){
            let n = Object.assign({},i);
            if(n.longitude && n.latitude){
              if(n.latitude > n.longitude){
                let tmp = n.latitude;
                n.latitude = n.longitude;
                n.longitude = tmp;
              }
              n.location = n.latitude + ',' + n.longitude;
              list.push(n);
            }
          }
          this.setData({ tjz: list, });
          this.orderTJZ();
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  //判断城市,之后体检站不同城市需要实现功能
  whichCityIAm(e) {
    let t = cityBL(e.detail.location.lng + ',' + e.detail.location.lat);
    // console.log(t);
  },
  // 根据距离排序
  orderTJZ: function () {
    let list = [];
    for (let i of this.data.tjz) {
      const t = {
        latitude: i.latitude,
        longitude: i.longitude,
      }
      list.push(t)
    }
    this.data.map.distanceToDestin(list, (d) => {
      if (d.message === 'query ok') {
        let list = d.result.elements.concat([]);
        for (let i = 0; i < list.length; i++) { // 根据距离排序
          for (let j = i + 1; j < list.length; j++) {
            if (list[i].distance > list[j].distance) {
              let tmp = list[j];
              list[j] = list[i];
              list[i] = tmp;
            }
          }
        }
        let tmp = this.data.tjz;
        let orderDistanceList = [];
        for (let i of list) {
          let testStr = i.to.lat + ',' + i.to.lng;
          for (let j of tmp) {
            if (j.location === testStr) {
              if (i.distance >= 1000) {
                j.distance = (i.distance / 1000).toFixed(1) + 'km';
              } else {
                j.distance = i.distance + 'm';
              }
              orderDistanceList.push(j);
              break;
            }
          }
        }
        orderDistanceList[0].active = true;
        this.setData({
          tjz: orderDistanceList,
        }, () => {
          this.markerTjz();
        })
      }
    }, (e) => {
      console.error(e);
    }, () => {

    })
  },
  showTJZ: function (e) {
    const add = e.currentTarget.dataset.address;
    if (add) {
      for (let i of this.data.tjz) {
        if (add === i.address) {
          const _point = i.location.split(',');
          const point = {
            latitude: _point[0],
            longitude: _point[1],
          }
          let orderDistanceList = this.data.tjz;
          for (let i of orderDistanceList) {
            i.active = false;
          }
          orderDistanceList[e.currentTarget.dataset.index].active = true;
          this.setData({
            tjz: orderDistanceList,
          })
          this.data.map.centerPoint(point);
          this.data.map.showPopupAboveCenter({
            name: i.name,
            address: i.address,
            latitude: _point[0],
            longitude: _point[1],
          });
          break;
        }
      }
    }
  },
  markerTjz: function () {
    let list = [];
    for (let i of this.data.tjz) {
      const _point = i.location.split(',');
      const point = {
        latitude: _point[0],
        longitude: _point[1],
        content: i.name,
        addr: i.address,
      }
      list.push(point);
    }
    this.data.map.markerPoints(list);
  },
  toTjzDetail(e){
    const name = e.currentTarget.dataset.name;
    for(let i of this.data.tjz){
      if( i.name === name ){
        wx.navigateTo({
          url: './tijianDetail?name=' + i.name + '&addr=' + i.address + '&location=' + i.location,
        })
        break;
      }
    }
  },
  showFrais(){
    wx.showModal({
      title: '',
      content: '体检费用：38.5元',
      showCancel: false,
      confirmText: '知道了',
      confirmColor: '',
    })
  },
  showLiucheng(){
    wx.showModal({
      title: '',
      content: '检测项目：视力、听力、辨色、手腕平衡感; 体检合格后领取体检表，贴1张一寸白底照片。',
      showCancel: false,
      confirmText: '知道了',
      confirmColor: '',
    })
  }
})