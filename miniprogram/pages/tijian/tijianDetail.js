// pages/tijian/tijianDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mapKey: 'HTQBZ-FN3WI-IIRG7-5JWI4-74OFZ-AYFFJ',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let point = this.data.location.split(',');
    point = {
      longitude: parseFloat(point[1]),
      latitude: parseFloat(point[0]),
    }
    this.selectComponent('#customMap').centerPoint(point, 16);
    let list = [];
    const _point = {
      latitude: point.latitude,
      longitude: point.longitude,
      content: this.data.name,
      addr: this.data.addr,
    }
    list.push(_point);
    this.selectComponent('#customMap').markerPoints(list);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  whichCityIAm: function () {

  },
  makePath:function(){
    let point = this.data.location.split(',');
    wx.openLocation({
      latitude: Number(point[0]),
      longitude: Number(point[1]),
      scale: '16',
      name: this.data.name,
      address: this.data.addr,
    })
    return;
    let e = {
      currentTarget:{
        dataset:{
          lat:Number(point[0]),
          lng:Number(point[1]),
        }
      }
    }
    this.selectComponent('#customMap').showNavToTjz(e)
  },
})