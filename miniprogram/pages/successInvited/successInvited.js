// pages/successInvited/successInvited.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentIndex: 0,
    attendListLength: 0,
    shareListLength: 0,
    shareList: [],
    shareCount: 0,
    attendCount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getSharePromise().then(res=>{
      console.info(res);
      if(res.data.code == 0 && res.data.message == '成功'){
        let _list = [];
        for (let i of res.data.data.shareList){
          if(i.subject){
            _list.push(i);
          }
        }
        this.setData({
          shareList: res.data.data.shareList,
          shareCount: res.data.data.shareCount,
          attendCount: res.data.data.attendCount,
          attendList: _list,
        })
      }
    }).catch(err=>{ 
      console.error(err);
    })
  },
  onShow: function(){

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  changeNav(e) {

    if (e.detail.text == "成功邀请") {
      this.setData({
        currentIndex: 0
      })
    } else if (e.detail.text == "成功报名") {
      this.setData({
        currentIndex: 1
      })
    }
  },
  getSharePromise(){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: 'https://www.haitunxueche.com/student/getShareVo',
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (err) => {
          reject(err);
        },
        complete: (e) => {

        }
      })
    })
  }
})