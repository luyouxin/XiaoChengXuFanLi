// pages/myOrder/myOrder.js
const dayjs = require('../../utils/day.js');
Page({

  data: {
    list: null,
    teacherName:'',
  },

  onLoad: function (options) {
    this.setData({
      teacherName: getApp().globalData.userStudyInfo.teacherName ,
    })
  },
  onShow: function () {
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/listOrderVo',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        console.log(res);
        if (res.data.code == 0) {
          let i = res.data.data;
          // pai xu
          for (let j = 0; j < i.length; j++) {
            for (let k = j + 1; k < i.length; k++) {
              if (i[j].createDate < i[k].createDate) {
                let tmp = i[k];
                i[k] = i[j];
                i[j] = tmp;
              }
            }
          }
          ///
          this.extraFunction(res.data.data);
          for (let j of i) {
            j.money = j.orderMoney.toFixed(2);
            /**1-待支付 PAY_WAIT, /**2-已支付 PAY_OVER,/**3-申请退款REQ_REFUND,4-已经退款REFUND_OVER,/**5-已入账OVER,/**6-已取消CANCEL */
            j.payText = j.payStatus == 1 ? '待支付' : j.payStatus == 2 ? '支付完成' : j.payStatus == 3 ? '退款' : j.payStatus == 6 ? '已取消' : j.payStatus == 5 ? '支付完成' : '';
            j.createDate = dayjs(j.createDate).format("YYYY-MM-DD HH:mm:ss");
          }
          this.setData({
            list: i,
          })
        }
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  onReady: function () {

  },
  failLoad: function (d) {
    wx.showModal({
      title: '读取失败',
      content: JSON.stringify(d.data),
      showCancel: false,
      confirmText: '',
      confirmColor: '',
      success: function (res) {
        wx.navigateBack();
      },
    })
  },
  extraFunction(list) { // 自动取消没有支付的订单，容错用
    let now = Date.now();
    for (let i of list) {
      if (i.payStatus === 1 && ((now - i.createDate) > 120000)) {
        wx.request({
          url: 'https://www.haitunxueche.com/student/index/closeOrder',
          data: {
            orderNumber: i.orderNumber,
          },
          header: {
            Authorization: getApp().globalData.login.token,
          },
          method: 'GET',
          success: function (res) {
            if (res.data.code === 0) {
              console.log('取消未支付订单：' + i.orderNumber);
            }
          },
          fail: function (res) { },
          complete: function (res) { },
        })
      }
    }
  }
})