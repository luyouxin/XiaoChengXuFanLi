const parser = require('paserContent.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    html: '',
    content:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    this.setData({
      id:options.id,
      time: (options.time.split(' '))[0]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.request({
      url: 'https://www.haitunxueche.com/common/getArticleInfoVo',
      data: {
        id:this.data.id,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      responseType: 'text',
      success: (res) =>{
        parser(res.data.data.content, this);
        this.setData({
          html: res.data.data.content
        })
        wx.setNavigationBarTitle({
          title: res.data.data.title,
        })
        return;
        // console.log(res.data.data.content);
        if(res.data.code===0){
          let _html = res.data.data.content.replace('<!--HTML-->','');
           _html = _html.replace('<br/>',`
           `);

          let t = _html.split('</p>');
          let _p = [];
          for(let i of t){
            // console.log('i',i);
            let po = i.indexOf('>')+1;
            let cc = i.slice(po,i.length);
            _p.push(cc);
          }
          // console.log(_p);
          let p = [];
          for(let i of _p){
            if(i.indexOf('<span')>=0){
              let point = i.indexOf('>')+1;
              let c = i.slice(point,i.length);
              c = c.replace('</span>','');
              p.push({
                type:'text',
                text:c
              });
            } else if(i.indexOf('<img')>=0){
              let t = i.slice(i.indexOf('"')+1,i.length);
              let t2 = t.slice(0, t.indexOf('"'));
              // console.log(t);
              // console.log(t2);
              p.push({
                type:'img',
                src:'http://www.haitunxueche.com:8980' + t2,
              })
            } else{
              p.push({
                type:'text',
                text:i,
              })
            }
          }

          this.setData({
            html:p,
            title:res.data.data.title
          })
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })
    this.didUserFavorite();
  },

  didUserFavorite(){ // 检查是否已经关注
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/listAttentionArticle',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) =>{
        if(res.data.code===0){
          if(res.data.data.length===0){
            this.setData({
              favorState:'no',
            })
          }else{
            for(let i of res.data.data){
              if(i.id === this.data.id){
                this.setData({
                  favorState: 'yes',
                })
                break;
              }
            }
            if (!this.data.favorState){
              this.setData({
                favorState: 'no',
              })
            }
          }
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  shareIt:function(e){
    wx.showShareMenu({
      withShareTicket: true,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  clickFocus:function(e){
    const id = this.data.id;
    const type = 'ARTICLE'
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/attention',
      data: {
        id:id,
        type:type,
      },
      header: {
        Authorization:getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) =>{
        if(res.data.code==0){
          wx.showToast({
            title: '添加关注成功',
            icon: '',
            duration: 1300,
            mask: true,
          })
        }        
      },
      fail: function(res) {},
      complete: (res) =>{
        setTimeout(() => {
          this.didUserFavorite()
        }, 1000)
      },
    })
  },
})