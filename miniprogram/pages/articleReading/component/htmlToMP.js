// pages/articleReading/component/htmlToMP.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    html:{
      type: Object,
      value:'',
      observer(n,o,p){
        if(!n){
          return;
        }
        // console.log(n);
        let style='';
        let val;
        if (n.tagType === 'block'){
          let target = n ;
          while( target.nodes){
            console.log(target)
            let tmp = '';
            if (target.styleStr){
              // tmp = target.styleStr.split(':')[1];
              // if(tmp){
              //   tmp = tmp.trim();
              // }
              style += target.styleStr;
            }
            target = target.nodes[0];
          }
          // console.log(target)
          if(target.node === 'text'){
            val = target.text;
          }
          console.log(style,val);
          this.setData({
            val:val,
            style:style,
          })
        }
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
