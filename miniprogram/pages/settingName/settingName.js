// pages/settingName/settingName.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    val: ''
  },
  onLoad: function (options) {
    console.log(getCurrentPages());
    console.log(getCurrentPages()[0]);
    let name = getApp().globalData.userInfo.nickName;
    let name2 = getApp().globalData.userWxInfo.nickName;
    if(name == null){
      this.setData({
        val: name2,
      });
    }else{
      this.setData({
        val: name,
      });
    }
  },
  clearFn(){
    this.setData({
      val: '',
    })
  },
  bindConfirm(e){
    console.log(e);
    var obj = Object.assign({}, getApp().globalData.userInfo);
    obj.nickName = e.detail.value;
    wx.request({
      url: 'https://www.haitunxueche.com/student/updateStudentInfo',
      data: obj,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      dataType: 'json',
      success: (res) => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          getApp().globalData.userInfo.nickName = e.detail.value;
          // getApp().globalData.userWxInfo.nickName = e.detail.value;
          this.setData({
            checked: 1,
          });
          wx.showToast({
            title: '姓名修改成功',
            icon: "loading"
          })
          setTimeout(function () {
            wx.navigateBack({
              delta: 1,
            })
            getCurrentPages()[1].onLoad();
            getCurrentPages()[0].onLoad();
          }, 2100);
        }else{

        }
      }
    })
  }
})