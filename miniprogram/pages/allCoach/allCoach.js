// pages/allCoach/allCoach.js
Page({

  data: {
    branchCoachList: [],
  },

  onLoad: function(options) {
    //获取分校教练列表
    this.getBranchListPromise(options.tid).then(res => {
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        const newArr = res.data.data;
        console.log(newArr);
        for(let i of newArr){
          i.score = i.score.toFixed(1);
        }
        this.setData({
          branchCoachList: newArr,
        })
      }
    }).catch(err => {
      console.warn(err);
    })
  },

  onReady: function() {

  },

  onShow() {

  },
  //获取分校教练列表
  getBranchListPromise(_tid) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/listTeacherListVo',
        data: {
          id: _tid,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }
      })
    })
  },
  //选择老师
  handleTeacher(e) {
    console.log(e);
    let tid = e.currentTarget.dataset.tid;
    wx.navigateTo({
      url: '../coach/coach?tid=' + tid,
    })
  }
})