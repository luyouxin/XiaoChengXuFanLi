// pages/search/search.js
Page({
  data: {
    keyLists: [], // 接收搜索的内容
    searchData: '', // 输入的值
    historyLists: [],
    isShow: false,
    inpValue: '',
    schoolList: [
    ],

  },
  onLoad: function (options) {
    // this.showHistoryList();
    let list = wx.getStorageSync('searchpagehistory');
    if (list) {
      this.setData({
        schoolList: JSON.parse(list),
      })
    }
  },
  onShow: function () {

  },

  searchSchool(e) { // 搜索按钮
    console.log(e);
    const t = e.detail.value;
    if (t && t.length > 0) {
      this.requestSchoolAndTeacher(t);
    }
  },
  userInput(e) { // 每次输入
    console.log(e);
    let t = e.detail.value.trim();
    if (t && t.length > 0) {
      this.setData({
        inpValue: t,
      })
      this.requestSchoolAndTeacher(t);
    }
  },
  requestSchoolAndTeacher(name) { // 查询方法
    let app = getApp().globalData;
    let list = [];
    Promise.all([getTeacher(name),getSchool(name)])
    .then( d=>{
      console.log(d);
      for(let i of d){
        list = list.concat(i);        
      }
      this.setData({
        schoolList: list,
      })
    })
    .catch( e => {
      console.error(e);
    })
  },
  clearKw() {
    this.setData({
      inpValue: '',
    })
  },
  toSchool: function (e) {
    const obj = {
      name: e.currentTarget.dataset.name,
      id: e.currentTarget.dataset.id,
      type: e.currentTarget.dataset.type,
    }
    let list = wx.getStorageSync('searchpagehistory');
    if (list) {
      list = JSON.parse(list);
      let test;
      for (let i of list) {
        if (i.id === obj.id) {
          test = true // 不新加入
          break;
        }
      }
      if (!test) { // 新加入
        list.push(obj);
        wx.setStorageSync('searchpagehistory', JSON.stringify(list));
      }
    } else {
      let _t = [];
      _t.push(obj)
      wx.setStorageSync('searchpagehistory', JSON.stringify(_t));
    }
    if(obj.type==='s'){
      wx.navigateTo({
        url: '../drivingSchool/drivingSchool?tid=' + obj.id,
      })
    }else{
      wx.navigateTo({
        url: '../coach/coach?tid=' + obj.id,
      })
    }
  }
})

const getSchool = (name,resolv, rej) => {
  return new Promise((resolv, rej) => {
    wx.request({
      url: 'https://www.haitunxueche.com/common/listSearch',
      data: {
        name: name,
        type: 'SCHOOL'
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        // console.log(res);
        if (res.data.code === 0) {
          let list = [];
          for (let i of res.data.data) {
            i.type = 's';
            list.push(i);
          }
          resolv(list)
        }
      },
      fail: function (res) {
        rej(res);
      },
      complete: function (res) { },
    })
  })
}
const getTeacher = (name,resolv, rej) => {
  return new Promise((resolv, rej) => {
    wx.request({
      url: 'https://www.haitunxueche.com/common/listSearch',
      data: {
        name: name,
        type: 'TEACHER'
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        // console.log(res);
        if (res.data.code === 0) {
          let list = [];
          for(let i of res.data.data){
            i.type = 't';
            list.push(i);
          }
          resolv(list);
        }
      },
      fail: function (res) {
        rej(res);
      },
      complete: function (res) { },
    })
  })
}