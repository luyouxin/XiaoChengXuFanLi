module.exports.Occupe = function(){
  return new Promise(function(resolve,reject){
    wx.request({ // 读取排课可用信息
      url: 'https://www.haitunxueche.com/teacher/listBespoken',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        console.log(res.data.data.length, res.data.code)
        if (res.data.code === 0) {
          resolve(res) ;
        }else{
          reject(res);
        }
      },
      fail: function (res) { 
        reject(res);
      },
      complete: function (res) { },
    })
  })
}
module.exports.paikeCapacity = function(){
  return new Promise(function(resolve,reject){
    wx.request({ // 读取排课可用信息
      url: 'https://www.haitunxueche.com/student/index/getPlanDefaultVo',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        console.log(res.data.data.length, res.data.code)
        if (res.data.code === 0) {
          resolve(res.data.data);
        }else{
          reject(res);
        }
      },
      fail: function (res) {
        reject(res);
       },
      complete: function (res) {  },
    }); 
  })
}

module.exports.addBespokenSb2 = function(data){
  return  new Promise( (resolve,reject)=>{
    wx.request({
      url: 'https://www.haitunxueche.com/teacher/addBespoken?subject=2',
      data: data,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      success: (res) =>{
        if (res.data.code === 0) {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function(res) {
        reject(res);
      },
      complete: function(res) {},
    })
  })
}
module.exports.addBespokenSb3 = function(data){
  return  new Promise( (resolve,reject)=>{
    wx.request({
      url: 'https://www.haitunxueche.com/teacher/addBespoken?subject=3',
      data: data,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      success: (res) =>{
        if (res.data.code === 0) {
          resolve(res.data.data);
        } else {
          reject(res);
        }
      },
      fail: function(res) {
        reject(res);
      },
      complete: function(res) {},
    })
  })
}
module.exports.delCours = function(data){
  if(data.length===0){
    return true;
  }
  return new Promise((resolve, reject) => {
    wx.request({
      url: 'https://www.haitunxueche.com/teacher/delBespoken',
      data: {
        ids:encodeURI(data),
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code === 0) {
          resolve(res.data.data);
        } else {
          reject(res);
        }
      },
      fail: function (res) {
        reject(res);
      },
      complete: function (res) { },
    })
  })
}
// 学生获取排课列表
module.exports.listPlanVo = function(){ 
  return new Promise((resolve, reject) => {
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/listPlanVo',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code === 0) {
          resolve(res.data.data);
        } else {
          reject(res);
        }
      },
      fail: function (res) {
        reject(res);
      },
      complete: function (res) { },
    })
  })
}

// 学生已u约课
module.exports.readBespoken = function (resolv, reject) {
  return new Promise((resolv, reject) => {
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/pageBespokenDetailVo',
      data: {
        current: 1,
        page: 100,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code === 0) {
          resolv(res.data.data);
        } else {
          reject(res.data);
        }
      },
      fail: function (res) {
        reject(res.data);
      },
      complete: function (res) {
        // wx.hideLoading();
      },
    })
  })
}

// 获取单节课程价格
module.exports.getSplitPrice = function (resolv, reject) {
  return new Promise((resolv, reject) => {
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/getSplitPrice',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        resolv(res);
      },
      fail: function (res) { 
        reject(res);
      },
      complete: function (res) { },
    })    
  })
}