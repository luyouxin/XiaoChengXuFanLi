// pages/rdv/checkout.js
Component({
  properties: {
    choose: {
      type: Array, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: [], // 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function (newVal, oldVal, changedPath) {
        let _l = newVal;
        let _t = [];
        for(let i of _l){          
          _t.push({
            cnDay:i.day,
            time: i.timeStr,
            raw: 'cour' + i.date + 'id' + i.id,
            price: this.data.courPrice.toFixed(2),
          })
        }
        const total = _t.length * this.data.courPrice;
        this.setData({
          course:_t,
          totalPay:total
        });
      }
    },
    courPrice:Number,
  },

  /**
   * 组件的初始数据
   */
  data: {
    course:[],
    totalPay:0,
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {      
    },
    moved: function () { },
    detached: function () { },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    closeEv:function(e){
      this.triggerEvent('closeThisModal',{},{});
    },
    delteTime:function(e){ // 删除这个时间
      // console.log(e);
      const _raw = e.currentTarget.dataset.raw;
      if(!_raw){return};
      this.triggerEvent('deletetime',
      {
        time:_raw,
      },
      {
        bubbles:true,
      })
    },
    payForBespoken:function(e){
      // if (!(this.data.courPrice>0)){
      //   wx.showModal({
      //     title: '错误',
      //     content: '课程价格获取错误，不能约课',
      //     showCancel: false,
      //     confirmText: '确定',
      //     confirmColor: '',
      //   })
      //   return;
      // }
      wx.showLoading({
        title: '',
        mask: true,
      })
      this.triggerEvent('paycours',{
        price:this.data.totalPay,
      },{})
    }
  }
})
