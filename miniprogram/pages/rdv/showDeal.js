// pages/rdv/showDeal.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    filterDeal: {
      type: String,
      value: '',
      observer(n, o, p) {
        // console.log(n);
        this.filterFunc(n);
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    myDeal: null,
    test: 123,
    cancelRdvModal: false,
  },
  lifetimes: {
    attached: function () {
      // console.log(getApp().globalData)
      this.setData({
        overClass: getApp().globalData.userStudyInfo.overClass,
        totalClass: getApp().globalData.userStudyInfo.totalClass,
      })
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    filterFunc(order) {
      if(!this.data.myDeal){
        return;
      }
      if (order === 'time') {
        let i = [];
        for (let i = 0; i < this.data.myDeal.length - 1; i++) {
          for (let j = i + 1; j < this.data.myDeal.length; j++) {
            if (this.data.myDeal[j].bespokenDate > this.data.myDeal[i].bespokenDate ){
              let tmp = this.data.myDeal[i];
              this.data.myDeal[i] = this.data.myDeal[j];
              this.data.myDeal[j] = tmp;
            }
          }
        }
        this.setData({
          myDeal: this.data.myDeal,
        })
      } else {
        let i = [];
        for (let i = 0; i < this.data.myDeal.length - 1; i++) {
          for (let j = i + 1; j < this.data.myDeal.length; j++) {
            if (this.data.myDeal[j].createDate < this.data.myDeal[i].createDate) {
              let tmp = this.data.myDeal[i];
              this.data.myDeal[i] = this.data.myDeal[j];
              this.data.myDeal[j] = tmp;
            }
          }
        }
        this.setData({
          myDeal: this.data.myDeal,
        })
      }
    },
    requestList() {
      wx.showLoading({
        title: '读取信息中..',
        mask: true,
      })
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/pageBespokenDetailVo',
        data: {
          current: 1,
          page: 100,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success: (res) => {
          if (res.data.code === 0) {
            console.log(res.data.data);
            this.setData({
              myDeal: res.data.data.records
            }, () => {
              this.orderList();
              this.renderData();
            });
          }
        },
        fail: function (res) { },
        complete: function (res) {
          wx.hideLoading();
        },
      })
    },
    renderData: function () {
      let o = this.data.myDeal;
      for (let i of o) {
        i.money = i.payMoney.toFixed(2);
        let createT = new Date(i.createDate);
        const c_month = createT.getMonth() + 1;
        i.cTime = createT.getFullYear() + '-' + (c_month > 9 ? c_month : '0' + c_month) + '-' + (createT.getDate() > 9 ? createT.getDate() : '0' + createT.getDate()) + ' ' + (createT.getHours() > 9 ? createT.getHours() : '0' + createT.getHours()) + ':' + (createT.getMinutes() > 9 ? createT.getMinutes() : '0' + createT.getMinutes());
        let cour = new Date(i.bespokenDate);
        let day;
        switch (cour.getDay()) {
          case 3:
            day = '周三';
            break;
          case 4:
            day = '周四';
            break;
          case 5:
            day = '周五';
            break;
          case 6:
            day = '周六';
            break;
          case 0:
            day = '周日';
            break;
          case 1:
            day = '周一';
            break;
          case 2:
            day = '周二';
            break;
        }
        let _month = cour.getMonth() + 1;
        let _time = cour.getHours();
        let _timeNext = parseInt(_time) + 1;
        const _date = cour.getDate();
        i.courTime = day + (_month > 9 ? _month : '0' + _month) + '.' + (_date > 9 ? _date : '0' + _date) + ' ' + (_time > 9 ? _time : '0' + _time) + ':00 ~ ' + (_timeNext > 9 ? _timeNext : '0' + _timeNext) + ':00';
        // 取消按钮
        let now = Date.now();
        if (now > i.bespokenDate) {
          i.cancelText = '已完成';
        } else if (now > (i.bespokenDate + 10 * 60 * 60 * 1000)) {
          i.cancelText = '不可取消';
        } else {
          i.cancelText = '取消预约';
        }
        if (i.orderStatus == 5) {
          i.cancelText = '已完成';
        } else if (i.orderStatus == 4) {
          i.cancelText = '已取消';
        } else if (i.orderStatus == 3) {
          i.cancelText = '取消预约中';
        } else if (i.orderStatus == 6) {
          i.cancelText = '未支付取消';
        }
      }
      // console.log('mydeal',o)
      this.setData({
        myDeal: o
      });
    },
    cancelRdv: function (e) {
      console.log('disabled' in e.currentTarget.dataset)
      if ('disabled' in e.currentTarget.dataset) {
        return;
      }
      this.setData({
        cancelRdvModal: true,
        activeDelteId: e.currentTarget.dataset.id,
      })
    },
    closeModalFunc: function (e) {
      this.setData({
        cancelRdvModal: false,
      })
    },
    yesFun(e) {
      if (this.activeDelteId) { return; }
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/cancelBespoken',
        data: {
          id: this.data.activeDelteId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success: (res) => {
          if (res.data.code == 0) {
            this.requestList();
          } else {
            wx.showModal({
              title: '操作失败',
              content: JSON.stringify(res.data),
              showCancel: false,
              confirmText: '关闭',
              confirmColor: '',
            })
          }
        },
        fail: function (res) {
          wx.showModal({
            title: '操作失败',
            content: JSON.stringify(res.data),
            showCancel: false,
            confirmText: '关闭',
            confirmColor: '',
          })
        },
        complete: (res) => {
          this.setData({
            cancelRdvModal: false,
            activeDelteId: null,
          })
        },
      })
    },
    orderList() {
      if (this.data.myDeal) {
        if (this.data.filterDeal === 'time') {
          let newList = this.data.myDeal;
          for (let i = 0; i < (newList.length - 1); i++) {
            for (let j = i + 1; j < newList.length; j++) {
              if (newList[i].bespokenDate > newList[j].bespokenDate) {
                let tmp = newList[j];
                newList[j] = newList[i];
                newList[i] = tmp;
              }
            }
          }
          let t = this.data.overClass;
          for(let i of newList){
            i._overclass = ++t;
          }
          this.setData({ myDeal: newList });
        } else {
          let newList = this.data.myDeal;
          for (let i = 0; i < (newList.length - 1); i++) {
            for (let j = i + 1; j < newList.length; j++) {
              if (newList[i].createDate < newList[j].createDate) {
                let tmp = newList[j];
                newList[j] = newList[i];
                newList[i] = tmp;
              }
            }
          }
          this.setData({ myDeal: newList });
        }
      }
    }
  }
})
