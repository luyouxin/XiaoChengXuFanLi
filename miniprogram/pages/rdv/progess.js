const userModule = require('../../utils/usermodule.js');
const studentSubjectCtrl = require('../../utils/studentCtrl.js').StudentSubjectCtrl;

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // insubject:{
    //   type:String,
    //   value:'',
    //   observer: function (newVal, oldVal, changedPath) {
    //     this.setState(newVal);
    //   }
    // }
  },

  /**
   * 组件的初始数据
   */
  data: {
    step: [{
        state: 'done',
        description: '完成体检'
      },
      {
        state: 'done',
        description: '驾校面签'
      },
      {
        state: 'done',
        description: '预约科目一'
      },
      {
        state: 'done',
        description: '通过考试'
      },
    ],
    manque: 0,
    showCustomToast: true, // 显示这个窗体
    showbody: '',
    finishSubject1: false, // 完成科目1的modal提示

  },
  lifetimes: {
    attached: function() {},
  },

  /**
   * 组件的方法列表
   */
  methods: {
    setState(sub) {
      let _manque;
      if (sub == '1' || sub == '10') {
        _manque = 4;
      } else if (sub == '11') {
        _manque = 3;
      } else if (sub == '12') {
        _manque = 2;
      } else if (sub == '13') {
        _manque = 1;
      }
      let tmp = this.data.step;
      for (let i = 0; i < 4; i++) {
        tmp[i].state = 'done'
      }
      for (let i = 0; i < _manque; i++) {
        tmp[3 - i].state = 'wait'
      }
      this.setData({
        manque: _manque,
        step: tmp,
      })
    },    
    clickProgress: function(e) {
      // console.log(this.data.manque);
      const good = (res) => {
        // console.log(res);
        this.triggerEvent('reloadClass', {}, {});
      }
      const bad = (res) => {
        wx.showToast({
          title: res,
          icon: 'none',
          duration: 2200,
          mask: true,
        })
      }
      const done = (res) => {
        wx.hideLoading();
      }
      wx.showLoading({
        title: '稍等',
        mask: true,
      })
      if (this.data.manque === 4) {
        // 11 - 完成体检
        studentSubjectCtrl('11', good, bad, done);
      } else if (this.data.manque === 3) {
        // 12 - 完成面签
        studentSubjectCtrl('12', good, bad, done);
      } else if (this.data.manque === 2) {
        // 13 - 完成预约科目一
        studentSubjectCtrl('13', good, bad, done);
      } else if (this.data.manque === 1) {
        // 最终完成,改为科目2状态可以约课
        studentSubjectCtrl('2', good, bad, done);
      }
    },    
  }
})