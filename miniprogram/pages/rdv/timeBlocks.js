// pages/rdv/timeBlocks.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    time: {
      type: Array,
      value: [],
      observer: function (newVal, oldVal, changedPath) {

      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    chooseTimes: [],
    touchStartPosition: null,
    testWidth: null,
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
      const width = wx.getSystemInfoSync().screenWidth;
      // console.log(width);
      this.setData({
        testWidth: width * 0.45,
      })
    },
    moved: function () { },
    detached: function () { },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    chooseTime: function (e) {
      // console.log(e.currentTarget);
      if (e.currentTarget.dataset.free === 'n' || e.currentTarget.dataset.status !== '1') {
        console.log(e.currentTarget.dataset);
        let str;
        if (e.currentTarget.dataset.rdvbyme) {
          str = '您已成功预约该时段';
        } else if (e.currentTarget.dataset.reason === '未排课') {
          str = '该时间段未排课';
        } else if (e.currentTarget.dataset.reason === '过期') {
          str = '该时间段已过期';
        } else if (e.currentTarget.dataset.reason === '被约') {
          str = '该时间段已被其他人预约';
        }
        wx.showToast({
          title: str,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
        return;
      }
      // 返回修改的列表
      this.triggerEvent('chooseTime', { id: e.currentTarget.dataset.id }, {});
    },

  }
})
