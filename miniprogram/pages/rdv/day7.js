// pages/rdv/day7.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    activeDate:String,    
    indate:Array,
    dateStruct:{
      type:Array,
      value: [], // 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function (newVal, oldVal, changedPath) {
        if(newVal && newVal.length >0){
          this.makeNext6Day(newVal);
        }
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    todayDate: '',
    next6Day: [],
    bluestyle: "color:#3972ff",
  },

  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
    },
    moved: function () { },
    detached: function () { },
  },


  /**
   * 组件的方法列表
   */
  methods: {
    numberToText(num) {
      if (num < 10) {
        return '0' + num.toString();
      }
      return num.toString();
    },
    numberToWeek(num) {
      switch (num) {
        case 3:
          return '周三'
        case 4:
          return '周四'
        case 5:
          return '周五'
        case 6:
          return '周六'
        case 0:
          return '周日'
        case 1:
          return '周一'
        case 2:
          return '周二'
      }
    },
    makeNext6Day(d) { // d 为date对象 初始用
      console.log(d);
      // const day = d;
      const tmp = [];
      for (let i of d) {
        let o = {};
        o.str = i.day;
        o.date = i.date.slice(5,i.date.length);
        o.datetime = i.date;
        o.total = i.total;
        tmp.push(o);
      }
      // console.log(tmp);
      this.setData({
        next6Day: tmp,
      });

    },
    changeDay: function (e) { // 点击切换日期
      this.triggerEvent('activeDate', { date: e.currentTarget.dataset.datetime }, {});
    }
  },
})
