module.exports = function(_this){
  wx.showLoading({
    title: '同步学习信息',
    mask: true,
  })
  const loadStudy = () => {
    wx.request({
      url: getApp().globalData.serverAddress + 'student/index/getClasses',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code === 0) {
          getApp().globalData.userStudyInfo = res.data.data;
        } else {
          getApp().globalData.userStudyInfo = null;
        }
        _this.firstStep();
      },
      fail: (res) => {
        getApp().globalData.userStudyInfo = null;
      },
      complete:(res)=>{
        wx.hideLoading();
      }
    })
  }
  loadStudy();
}