// pages/articleRead/articleRead.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navSelected:'热门',
    articles:[],
    pageSize:20,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.requestList(false);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.readingFunc(true); // 下一页
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  changeNavFunc(e){
    console.log(e);
    this.setData({
      navSelected:'最新'
    },()=>{
      this.requestList(false);
    })
  },
  readingFunc(e){
    var id = e.currentTarget.dataset.id;   
    wx.navigateTo({
      url: '../articleReading/articleReading?id=' + id + '&time=' + e.currentTarget.dataset.time,
    }) 
  },
  requestList(nextPage){
    wx.showLoading({
      title: '读取文章',
    });
    let page = 1;
    if ( (nextPage && this.data.current)  && this.data.current < this.data.pages){ 
      page = this.data.current + 1;
    }
    let sortType = 1;
    if(this.data.navSelected === '最新'){
      sortType = 2
    }
      wx.request({
        url: 'https://www.haitunxueche.com/common/pageArticle',
        data: {
          current:page,
          page:this.data.pageSize,
          sortType:sortType,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success:  (res) => {
          if(res.data.code===0){
            this.updateList(res.data.data);
          }
         },
        fail: function (res) { },
        complete: function (res) {
          wx.hideLoading();
         },
      })
    
  },
  updateList:function(dat){
    console.log(dat);
    if (dat.total <1){ // 空数据
      this.setData({
        articles:[]
      });
      return;
    };
    this.setData({
      current: dat.current,
      pages:dat.pages,
    });
    // 创建时间处理
    for(let i of dat.records){
      i.time = '';
      if (i.createDate){
        // 处理创建时间
        let t = new Date(i.createDate);
        let o = t.getFullYear() + '年' + (t.getMonth()+1) + '月' + t.getDate() + '日' + '  ' + t.getHours() + '点' + t.getMinutes() + '分';
        i.time = o;
      }      
    }
    //
    this.setData({
      articles:dat.records,
    })
  }
})