// pages/drivingSchool/component/coachBlock.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    areaid:{
      type:String,
      value:'',
      observer:function(n,o,p){
        if(n.length > 0){
          this.requestCoach();
        }
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },
  lifetimes: {
    attached() {      
    },
    detached() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    requestCoach(){
      console.log(this.data.areaid)
      wx.request({
        url: getApp().globalData.serverAddress + 'student/index/listTeacherListVo',
        data: {
          id: this.data.areaid,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success: (res) => {
          if (res.data.code === 0) {
            let list = res.data.data;
            const oss = getApp().globalData.ossLink;
            for (let i of list) {
              i.photo = oss + i.topPic + '?x-oss-process=image/resize,l_300';
            }
            let tmp = [];
            if(list.length>3){
              tmp.push(list.slice(0,3));
              tmp.push(list.slice(3,6));
            }else{
              tmp.push(list);
            }
            this.setData({ list: tmp, });
            for(let i of list){
              this.loadCoachEva(i.id);
            }
          } else {
            this.failLoadCoachList(res.data);
          }
        },
        fail: (res) => {
          this.failLoadCoachList(res)
        },
        complete: function (res) { },
      })
    },
    failLoadCoachList(err){
      let msg;
      if(typeof(err) === 'string'){
        msg = err;
      }else if(typeof(err) === 'object'){
        msg = err.message ? err.message : JSON.stringify(err);
      }else {
        msg = JSON.stringify(err);
      }
      wx.showToast({
        title: '教师列表错误: ' + msg,
        icon: 'none',
        image: '',
        duration: 1500,
        mask: true,
      })
    },
    toCoach(e){
      const id = e.currentTarget.dataset.id;
      wx.navigateTo({
        url: '/pages/coach/coach?tid=' + id,
      })
    },
    handleMoreCoach() {
      wx.navigateTo({
        url: '/pages/allCoach/allCoach?tid=' + this.data.areaid,
      })
    },
    loadCoachEva(id){
      wx.request({
        url: getApp().globalData.serverAddress + 'student/index/getEvaluateStatisticsVo',
        data: {
          id:id,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success: (res) => {
          if(res.data.code === 0){
            let sc = res.data.data.score.toFixed(1);
            for(let i of this.data.list){
              for(let j of i){
                if(j.id === id){
                  j._score = sc;
                  break;
                }
              }
            }
            this.setData({ list: this.data.list});
          }
        },
        fail: function(res) {},
        complete: function(res) {},
      })
    }
  }
})
