// pages/courDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    blockLeftDistance: 'margin-left: 98rpx;',
    switchState: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if (options.baoxian){
      this.setData({
        switchState:3,
        blockLeftDistance: 'margin-left: ' + (98 + 3 * 250).toString() + 'rpx;',
      })
    } else if (options.jssf){
      this.setData({
        switchState: 2,
        blockLeftDistance: 'margin-left: ' + (98 + 2 * 250).toString() + 'rpx;',
      })
    } else if (options.coachprice){
      this.setData({
        pfPrice: options.coachprice,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  switchFunc(e){
    let t = e.currentTarget.dataset.diff;
    t = Number(t);
    this.setData({
      blockLeftDistance: 'margin-left: ' + ( 98 + t * 250).toString() + 'rpx;',
      switchState: t,
    })
  }
})