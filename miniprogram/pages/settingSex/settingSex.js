// pages/settingPortrait/settingPortrait.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checked: 0,
  },
  onLoad: function (options) {
    console.log(options);
    console.log(app.globalData);
    console.log(getCurrentPages());
    let sex = options.sex;
    this.setData({
      checked: sex,
    })
  },
  onReady: function () {

  },
  handleMale(){
    var obj = Object.assign({}, getApp().globalData.userInfo);
    obj.gender = 1;
    wx.request({
      url: 'https://www.haitunxueche.com/student/updateStudentInfo',
      data: obj,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      dataType: 'json',
      success:(res)=>{
        console.log(res);
        if(res.data.code == 0 && res.data.message == "成功"){
          getApp().globalData.userInfo.gender = 1;
          // getApp().globalData.userWxInfo.gender = 1;
          // res.data.data.gender = 1;
          this.setData({
            checked: 1,
          });
          wx.showToast({
            title: '性别修改成功',
            icon: "loading"
          })
          setTimeout(function () {
            wx.navigateBack({
              delta: 1,
            })
            getCurrentPages()[1].onLoad();
          }, 2200);
        }
      }
    })
  },
  handleFemale(){
    var obj = Object.assign({}, getApp().globalData.userInfo);
    obj.gender = 0;
    wx.request({
      url: 'https://www.haitunxueche.com/student/updateStudentInfo',
      data: obj,
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'POST',
      dataType: 'json',
      success: (res) => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          getApp().globalData.userInfo.gender = 0;
          // getApp().globalData.userWxInfo.gender = 0;
          // res.data.data.gender = 0;
          this.setData({
            checked: 0,
          });
          wx.showToast({
            title: '性别修改成功',
            icon: "loading"
          })
          setTimeout(function () {
            wx.navigateBack({
              delta: 1,
            })
            getCurrentPages()[1].onLoad();
          }, 2200);
        }
      }
    })
  }
})