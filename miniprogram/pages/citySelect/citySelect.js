const areaList = require('../../lib/city.js').areaList;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    site:[
      { shortcut:'G',value:['桂林','贵州']},
      { shortcut: 'N', value: ['南宁', '那龙']},
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(areaList);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  clickSite: function(e){
    console.log(e.currentTarget.dataset.kw);
  },
  // 组件事件测试
  listenChild:function(e){
    console.log(e);
  },
  takeSite:function(e){
    let appInstance = getApp();
    let _cid = '450100'
    appInstance.globalData.userLocation.cid = _cid;
    appInstance.globalData.userLocation.city = '南宁';
    appInstance.globalData.userLocation.address = '';
    wx.switchTab({
      url: '../index/index',
    });
    return;
  }
})