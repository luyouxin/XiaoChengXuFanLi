// pages/home/home.js
var app = getApp();
var City = require('../../lib/city.js').areaList;

Page({

  data: {
    low: 0,
    heigh: 6000,
    low1: 0,
    heigh1: 5000,
    tempCityName: '',
    tempAreaListName: [],
    currentTab: 0,
    curTab: 0,
    licenseType: '',
    cityCode: null,
    licenseTypeArr: ['C1', 'C2', 'A1', 'A2', 'B2'],
    sexChoice: null, // 1 男 2女
  },

  onLoad: function (options) {
    this.handleValue();
    if(getCurrentPages()[0].data.showCoachList){
      this.setData({ indexShowArea: false })
    }else{
      this.setData({ indexShowArea: true });
    }
  },
  handleValue(_objArr, _options) {
    const pages = getCurrentPages();
    for (let i of pages) {
      if (i.route.indexOf('index/index') >= 0) {
        let inState;
        if (i.data.showCoachList) {
          inState = i.data.teacherOptions;
        } else {
          inState = i.data.schoolOptions;
        }
        this.setData({
          low1: inState.priceBegin ? parseInt(inState.priceBegin) : 0,
          heigh1: inState.priceEnd ? parseInt(inState.priceEnd) : 6000,
          sexChoice: i.data.sexFilter ? i.data.sexFilter : null,
          curTab: inState.cid ? inState.cid : null,
          cityCode: inState.cid ? inState.cid : null,
          licenseType: inState.courseType ? inState.courseType : null,
        })
      }
    }
    return;
    if (_objArr.length == 0) {
      this.setData({
        low1: 0,
        heigh1: 5000,
      })
    } else {
      // console.log(this.data.tempAreaListName);
      this.setData({
        low1: _options.leastValue ? parseInt(_options.leastValue) : 0,
        heigh1: _options.maximumValue ? parseInt(_options.maximumValue) : 5000,
        sexChoice: _options.sex ? _options.sex : null,
        curTab: _options.cityCode ? _options.cityCode : null,
        cityCode: _options.cityCode ? _options.cityCode : null,
        licenseType: _options.licenseType ? _options.licenseType : null,
      })
    }
  },
  onShow() {
    // console.log(app.globalData);
    let cid = app.globalData.userLocation.cid;
    if (cid) {
      for (let i of City) {
        for (let j of i.city) {
          if (j.code.toString() === cid.toString()) {
            this.setData({
              tempAreaListName: j.area,
              tempCityName: j.name,
            });
          }
        }
      }
    }
  },
  clickTab(e) { // 驾照类型
    this.setData({
      licenseType: e.target.dataset.value === this.data.licenseType ? null : e.target.dataset.value,
    })
  },
  clickTab2(e) {
    if (e.target.dataset.code == this.data.cityCode) {
      this.setData({
        curTab: 0,
        cityCode: 0,
      })
    } else {
      this.setData({
        curTab: e.target.dataset.code,
        cityCode: e.target.dataset.code,
      })
    }
  },
  handleFilter(e) { // 提交修改
    let obj = {
      priceBegin: this.data.low1,
      priceEnd: this.data.heigh1,
      courseType: this.data.licenseType || '',
      cid: this.data.cityCode || '',
    }
    let indexPage = getCurrentPages()[0];
    if (indexPage.data.showCoachList) {
      for (let i in obj) {
        indexPage.data.teacherOptionsNext[i] = obj[i];
      }
    } else {
      for (let i in obj) {
        indexPage.data.schoolOptionsNext[i] = obj[i];
      }
    }
    indexPage.setData({
      sexFilter: this.data.sexChoice ? this.data.sexChoice : null,
    })
    wx.navigateBack();
  },
  //滑竿事件
  lowValueChangeAction: function (e) {
    this.setData({
      low1: e.detail.lowValue
    })
  },

  heighValueChangeAction: function (e) {
    this.setData({
      heigh1: e.detail.heighValue
    })
  },

  lowValueChangeAction1: function (e) {
    this.setData({
      low1: e.detail.lowValue
    })
  },

  heighValueChangeAction1: function (e) {
    this.setData({
      heigh1: e.detail.heighValue
    })
  },

  hideSlider: function (e) {
    this.selectComponent("#zy-slider").hide()
    this.selectComponent("#zy-slider1").hide()
  },

  showSlider: function (e) {
    this.selectComponent("#zy-slider").show()
    this.selectComponent("#zy-slider1").show()
  },
  resetSlider(e) {
    this.selectComponent("#zy-slider").reset();

    getCurrentPages()[0].setData({
      sexFilter: null,
      teacherOptionsNext: { // 请求老师列表的初始值
        areaId: '',
        cid: '',
        courseType: '',
        latitude: '',
        longitude: '',
        priceBegin: 0,
        descOrAsc: 'DESC',
        orderBy: 'DISTANCE', //排序SCORE-评分，SALES-销量，DISTANCE-距离
        priceEnd: 5000
      },
      schoolOptionsNext: { // 请求学校列表的初始值
        areaId: '',
        cid: '',
        courseType: '',
        latitude: '',
        longitude: '',
        priceBegin: 0,
        priceEnd: 5000
      },
    });
    wx.navigateBack();
  },
  setSex(e) {
    let val = e.currentTarget.dataset.value;
    if (this.data.sexChoice == val) {
      this.setData({
        sexChoice: null,
      })
      return;
    }
    this.setData({
      sexChoice: val,
    })
  }
})