module.exports.flowVo = function(_this){
  wx.showLoading({
    title: '',
    mask: false,
  })
  wx.request({
    url: getApp().globalData.serverAddress + 'finance/getFinanceVo',
    data: '',
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    success: function(res) {
      if(res.data.code === 0){
        _this.setData({
          gainTotal: res.data.data.amount.toFixed(2),
        })
      }
    },
    fail: function(res) {},
    complete: function(res) {
      wx.hideLoading();
    },
  })
}
// 提现
module.exports.retirerAgent = function (_this, amount,cb){
  wx.request({
    url: getApp().globalData.serverAddress + 'finance/withdraw',
    data: {
      amount: amount,
    },
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    success: function (res) {
      // console.log(res);
      if(res.data.code === 0){
        wx.showToast({
          title: '操作成功',
          icon: '',
          image: '',
          duration: 2000,
          mask: true,
        })
      }else{
        wx.showModal({
          title: '操作失败',
          content: res.data.message,
          showCancel: false,
          confirmText: '关闭',
        })
      }
      setTimeout(function(){
        if(cb){
          cb();
        }
      },2000);
    },
    fail: function (res) { 
      console.warn(res);
      wx.showModal({
        title: '操作失败',
        content: JSON.stringify(res),
        showCancel: false,
        confirmText: '关闭',
      })
    },
    complete: function (res) { },
  })
}
