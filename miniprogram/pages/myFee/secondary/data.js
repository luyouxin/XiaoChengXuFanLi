module.exports.pageSchoolRecord = function (_this, callback,recordType,startDate,endDate,page = 1, pageSize = 600){
  const failFunc = function (d) {
    wx.showModal({
      content: d,
      showCancel: false,
      confirmText: '关闭',
      confirmColor: '',
    })
  };
  wx.showLoading({
    title: '',
    mask: true,
  });
  wx.request({
    url: getApp().globalData.serverAddress + 'finance/pageSchoolRecord',
    data: {
      recordType: recordType,
      startDate: startDate,
      endDate: endDate,
      page: page,
      pageSize: pageSize,
    },
    header: {
      Authorization: getApp().globalData.login.token,
    },
    method: 'GET',
    dataType: 'json',
    success: function(res){
      if (res.data.code === 0) {
        let list = res.data.data.records;
        console.log(list);
        let lastList = [];
        for( let i of list ){
          let tmp = new Date(i.creatDate);
          i.ct = tmp.getFullYear() + '年' + ( tmp.getMonth() +1 ) + '月' + tmp.getDate() + '日';
          i.ctTime = tmp.getHours() + ':' + tmp.getMinutes();
          let sucessInsert = false;
          for(let j of lastList){
            if(j.date === i.ct){
              j.data.push(i);
              sucessInsert = true;
              j.amount += i.amount;
              break;
            }
          }
          if(!sucessInsert){
            let obj = {
              date: i.ct,
              data:[i],
              amount: i.amount,
            }
            lastList.push(obj)
          }
        }
        for(let i of lastList){
          i.amount = i.amount.toFixed(2);
        }
        _this.setData({
          records: lastList,
        })
        console.log(_this.data);
      } else {
        failFunc(res.message + ',' + res.data);
      }
      if(callback){
        callback();
      }
    },
    fail: function (res) {
      failFunc(res);
    },
    complete: function (res) {
      wx.hideLoading();
    },
  })
}