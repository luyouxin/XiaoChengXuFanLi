const pageSchoolRecord = require('data.js').pageSchoolRecord;
const dayjs = require('../../../utils/day.js');

Page({

  data: {
    tradeType: [],
    recordType: 1, // 收支类型：1-收入，2-支出
    start:null,
    end:null,
  },

  onLoad: function (options) {
    this.setData({
      income: options.income || null,
    })
    let position = getApp().globalData.userInfo.position;
    if(position == 3){
      this.setData({ tradeType: ['约课费用', '提现记录'], });
    } else if (position == 100) {
      this.setData({ tradeType: ['报名费用', '提现记录'], });
    } else {
      this.setData({ tradeType: ['赏金', '提现记录'], });
    };
  },

  onReady: function () {
    this.getHistory();
  },

  onShow: function () {
  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  toogleFilter(e) {
    this.setData({
      filterOpen: !this.data.filterOpen,
    })
  },

  getHistory() {    // 资金流水    
    if(!this.data.start){ // 没有设置起始时间就由两个月前开始
      let day = dayjs();
      day = day.set('hour', 0).set('minute', 0).set('second', 0).set('millisecond', 0);  
      day = day.subtract(2, 'month');
      console.log(day.format('YYYY-MM-DD HH-mm'));
      this.setData({
        start: day.format('YYYY-MM-DD HH:mm:ss'),
      });
    }
    if(!this.data.end){
      let day = dayjs();
      this.setData({
        end: day.format('YYYY-MM-DD 23:59:59'),
      });
    }
    pageSchoolRecord(this, this.orderByDate, this.data.recordType,this.data.start,this.data.end);
  },
  orderByDate() {
    for (let i = 0; i < this.data.records.length - 1; i++) {
      for (let j = i +1; j < this.data.records.length; j++) {
        if (this.data.records[i].data[0].creatDate < this.data.records[j].data[0].creatDate){
          let tmp = this.data.records[i];
          this.data.records[i] = this.data.records[j];
          this.data.records[j] = tmp;
        }
      }
    }
    this.setData({
      records: this.data.records,
    })
  },
  typeChange(e){
    const type = e.detail.value;
    console.log(type); // type返回值 0收入，1提款
    if(type == 0){
      this.setData({ recordType: 1 });
    }else{
      this.setData({ recordType: 2 });
    };
    this.getHistory();
  },
  queryData(e){
    console.log(e.detail);
    let start = e.detail.start;
    let end = e.detail.end;
    let _start = start ? dayjs(start) : null;
    let _end = end ? dayjs(end) : null;
    if(_end && !_start){
      wx.showToast({
        title: '没有开始时间',
        icon: 'none',
        duration: 2000,
        mask: true,
      })
      return;
    };
    if(_end && _start){
      if ( !(_end.isAfter(_start)) ){
        wx.showToast({
          title: '开始时间晚于截止时间',
          icon: 'none',
          duration: 2000,
          mask: true,
        })
        return;
      }
    }
    this.setData({
      start: _start.format('YYYY-MM-DD HH:mm:ss'),
      end: _end ? _end.format('YYYY-MM-DD 23:59:59') : null,
    });
    this.getHistory();
  },
  // retirerHistroy(_this, page = 1, pageSize = 60) {  // 申请提现记录
  //   const failFunc = function (d) {
  //     wx.showModal({
  //       content: d,
  //       showCancel: false,
  //       confirmText: '关闭',
  //       confirmColor: '',
  //     })
  //   };
  //   wx.showLoading({
  //     title: '',
  //     mask: true,
  //   });
  //   wx.request({
  //     url: getApp().globalData.serverAddress + 'finance/pageSchoolWithdraw',
  //     data: {
  //       page: page,
  //       pageSize: pageSize,
  //     },
  //     header: {
  //       Authorization: getApp().globalData.login.token,
  //     },
  //     method: 'GET',
  //     dataType: 'json',
  //     success: (res) => {
  //       if (res.data.code === 0) {
  //         this.setData({
  //           records: res.data.data.records,
  //         })
  //       } else {
  //         failFunc(res.message + ',' + res.data);
  //       }
  //     },
  //     fail: function (res) {
  //       failFunc(res);
  //     },
  //     complete: function (res) {
  //       wx.hideLoading();
  //     },
  //   })
  // },
})