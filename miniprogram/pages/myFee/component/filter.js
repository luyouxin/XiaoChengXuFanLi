const dayjs = require('../../../utils/day.js');

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    startDate:null,
    startTime:null,
    endDate:null,
    endTime:null,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    chooseStartDate(e){
      console.log(e.detail.value);
      this.setData({ startDate: e.detail.value, });
    },
    chooseStartTime(e){
      console.log(e.detail.value);
      this.setData({ startTime: e.detail.value, });
    },
    chooseEndTime(e){
      console.log(e.detail.value);
      this.setData({ endTime: e.detail.value, });
    },
    chooseEndDate(e){
      console.log(e.detail.value);
      this.setData({ endDate: e.detail.value, });
    },
    sumbit(){
      this.triggerEvent('queryData',{
        start: this.data.startDate,
        end: this.data.endDate,
      },{});
    }
  }
})
