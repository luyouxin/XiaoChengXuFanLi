const flowVo = require('moneyFlow.js').flowVo;
const retirerAgent = require('moneyFlow.js').retirerAgent;

Page({

  data: {
    currentIndex: 0,
    page: 1,
    size: 20,
    loading: false,
    hasMore: false,
    currentTab: 20,
    inputValue: '',

    totalGain: '0.00',
    liquid: [],
    // 提现记录部分
    withdraw:{},
    // 可提现金额
    gainTotal: '0.00',
    moneyValue: [20,50,100],
  },

  onLoad: function (options) {
    this.requestLiquid();
    let amount = getApp().globalData.userInfo.amount;
    if(amount){
      this.setData({
        gainTotal: amount.toFixed(2),
      })
    }
  },

  onReady: function () {

  },
  changeNav(e) {

    if (e.detail.text == "我的收入") {
      this.setData({
        currentIndex: 0
      })
    } else if (e.detail.text == "微信提现") {
      this.setData({
        currentIndex: 1
      })
    }

  },
  handleDrawMoney() {
    wx.navigateTo({
      url: '../../pages/drawMoney/drawMoney',
    })
  },
  handleWithdraw(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.current,
    })
  },
  handleInput(e) {
    this.setData({
      inputValue: e.detail.value,
    })
  },
  requestLiquid() {
    flowVo(this);
  },

  userCallWithdraw(e){
    let money = this.data.currentTab;
    money = parseInt(money, 10);
    if( money > parseInt(this.data.gainTotal, 10)){
      wx.showToast({
        title: '提现金额大于结余',
        icon: 'none',
        duration: 2500,
        mask: true,
      })
      return;
    }
    retirerAgent(this,money,this.requestLiquid);
  },
  toList(){
    wx.navigateTo({
      url: './secondary/historyList?income=true',
    })
  },
  toList2(){
    wx.navigateTo({
      url: './secondary/historyList',
    })
  },
  toRetirer(){
    const tabs = this.selectComponent('#tabs');
    // console.log(tabs)
    tabs.changeTitle(
      {
        currentTarget:{
          dataset:{
            text: '微信提现',
          }
        }
      }
    )
  }
})