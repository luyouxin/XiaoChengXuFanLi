// pages/coach/coach.js
var app = getApp()
var fileData = require('../../utils/data.js');
var Pay = require('../../lib/pay.js');
var Deal = require('../../lib/deal.js');
var QQMapWX = require('../../lib/qqmap-wx-jssdk.min.js');
const WxParse = require('../../lib/wxParse/wxParse.js');

const userModule = require('../../utils/usermodule.js');

Page({

  data: {
    tabsData: fileData.getCoachNavData(),
    evaluateData: fileData.evaluateData(),
    // slideTabsData: fileData.slideTabsData(),
    slideTabsData: [],
    commentData: fileData.commentData(),
    currentTab: 0,
    currentType: 0,
    curNavId: 1,
    picUrls: '',
    modalCtrl: 'hidden',
    modalCtrl2: 'hidden',
    scrollHeight: 0,
    scrollTp: 0,
    toView: '',
    name: '',
    picSrc: '',
    briefIntroduction: '',
    areaName: '',
    classesList: [],
    drivingSchoolName: '',
    schoolAddress: '',
    lat1: '',
    lng1: '',
    lat2: '',
    lng2: '',
    distance: 0,
    introduction: '',
    teacherId: '',
    sex: 1,
    schoolAreaId: '',
    address: '',
    downPayment: '',
    orderId: '',
    piccActive: false,
    attention: true,
    schoolName: '',
    score: 0,
    score1: '',
    fans: 0,
    sales: 0,
    lifePicsLength: 0,
    workPicsLength: 0,
    totalPicsLength: 0,
    headerSchool: 0,
    studentListLength: 0,
    studentListLength1: 0,
    studentList: [],
    buyInsurance: false,
    badRate: 0,
    goodRate: 0,
    midRate: 0,
    total: 0,
    allLength: 0,
    goodLength: 0,
    middleLength: 0,
    lowLength: 0,
    allArr: [],
    goodArr: [],
    middleArr: [],
    lowArr: [],
    allTotal: 0,
    goodTotal: 0,
    middleTotal: 0,
    lowTotal: 0,
  },
  testModal() {
    this.setData({
      modalCtrl: 'show'
    })
  },
  onCloseEvent() {
    this.setData({
      modalCtrl: 'hidden'
    })
  },
  testModal2() {
    const userCurrentSubject = getApp().globalData.userInfo.subject || 0;
    if (userCurrentSubject >= 1) {
      wx.showModal({
        title: '重复报名',
        content: '报名后不能再次报名学车',
        showCancel: false,
        confirmText: '确定',
        confirmColor: '',
      })
      return;
    }
    this.setData({
      modalCtrl2: 'show'
    })
  },
  onCloseEvent2() {
    this.setData({
      modalCtrl2: 'hidden'
    })
  },

  onLoad: function(options) {
    
    console.log(options);
    //获取教练学员列表
    this.getStudentListPromise(options.tid).then(res=>{
      // console.log(res);
      if(res.data.code == 0 && res.data.message == "成功"){
        let _arr = [];
        let dataArr = res.data.data;
        if(dataArr.length == 1){
          for(let i = 0; i< 1; i++){
            _arr.push(dataArr[i]);
          }
        }else if(dataArr.length == 2){
          for(let i = 0; i< 2; i++){
            _arr.push(dataArr[i]);
          }
        } else if (dataArr.length >= 3 && dataArr.length < 10000){
          for(let i = 0; i< 3; i++){
            _arr.push(dataArr[i]);
          }
        }
        this.setData({
          studentListLength: res.data.data.length,
          studentList: _arr,
          studentListLength1: _arr.length,
        })
      }
    }).catch(err=>{
      console.warn(err);
    })
    //获取图片数量
    this.getPicsPromise(options.tid).then(res => {
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        let lifePicsLength = res.data.data.lifePics.length;
        let workPicsLength = res.data.data.workPics.length;
        let totalPicsLength = lifePicsLength + workPicsLength;
        this.setData({
          totalPicsLength: totalPicsLength,
        })
      } else {
        console.error(res);
      }
    }).catch(reason => {
      console.log(reason);
    });
    // 获取是否关注
    this.getIsAttention(options.tid).then(res=>{
      console.log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        this.setData({
          attention: res.data.data.attention,
          fans: res.data.data.count,
        })
      }
    }).catch(e=>{
      console.warn(e);
    });

    this.setData({
      teacherId: options.tid,
    })
    //请求场地
    let p2 = this.getInfoPromise(options.tid);
    p2.then((res) => {
      if (res.data.code == 0 && res.data.message == '成功') {
        this.setData({
          name: res.data.data.name,
          picSrc: res.data.data.topPic,
          briefIntroduction: res.data.data.briefIntroduction,
          areaName: res.data.data.areaName,
          drivingSchoolName: res.data.data.schoolName,
          // introduction: res.data.data.introduction,
          sex: res.data.data.sex,
          schoolAreaId: res.data.data.schoolAreaId,
          sales: res.data.data.sales,
          classesList: res.data.data.classesList,
          currentTab: res.data.data.classesList[0].id,
          downPayment: res.data.data.classesList[0].firstPrice,
          // downPayment: res.data.data.classesList[0].pfPrice,
          orderId: res.data.data.classesList[0].id,
        })
        //
        let introString = (res.data.data.introduction).toString();
        introString = introString.replace('<!--HTML-->','');
        introString = introString.replace('<p>','');
        introString = introString.split('</p>');
        this.setData({ introduction: introString });
        // WxParse.wxParse('article', 'html', this.data.introduction, this, 5);
      }
      let branchSchoolId = res.data.data.schoolAreaId;
      this.getBranchSchoolInfo(branchSchoolId).then((result) => {
        // console.log(result);
        if (result.data.code == 0 && result.data.message == "成功") {
          this.setData({
            address: result.data.data.address,
            schoolName: result.data.data.name,
            lat1: result.data.data.latitude,
            lng1: result.data.data.longitude,
            headerSchool: result.data.data.schoolId,
          })
          let p1 = this.getLocationPromise();
          p1.then((res) => {
            console.log(res);
            this.setData({
              lat2: res.latitude,
              lng2: res.longitude,
            })
            // console.log(this.data.lat2)
            // console.log(this.getDistance(this.data.lat1, this.data.lng1, this.data.lat2, this.data.lng2));
            let distanceNum = this.getDistance(this.data.lat1, this.data.lng1, this.data.lat2, this.data.lng2);
            this.setData({
              distance: (distanceNum / 1000).toFixed(1),
            })
          }).catch((error) => {

          })
        }
      }).catch((error) => {
        console.log(error);
      });

    }).catch(function(error) {
      console.error(error);
    });
    wx.getSystemInfo({
      success: (result) => {
        this.setData({
          scrollHeight: result.windowHeight,
        });
      },
      fail: () => {},
      complete: () => {}
    });

    //全部,好评,中评,差评
    this.getEvaluatePromise(1, 10, options.tid, 'ALL').then(res=>{
      // console.log(res);
      if(res.data.code == 0 && res.data.message == "成功"){
        const _allArr = res.data.data.records;
        let _allArr1 = [];
        for(let i of _allArr){
          let o = Object.assign({},i);
          o.time = this.timestampToTime(i.createDate).split(' ')[0].split('-').join('.');
          _allArr1.push(o);
        }
        this.setData({
          allArr: _allArr1,
          allTotal: res.data.data.records.length,
        })
      }
    }).catch(err=>{
      console.warn(err);
    });
    //获取评价评分
    this.getAverageScore(options.tid).then(res=>{
      // console.log(res);
      if(res.data.code == 0 && res.data.message == "成功"){
        let _badRate = Math.round((res.data.data.badRate)*100);
        let _goodRate = Math.round((res.data.data.goodRate) * 100);
        let _midRate = Math.round((res.data.data.midRate) * 100);
        let _score = Math.round(res.data.data.score).toFixed(1);
        let _score1 = Math.round(res.data.data.score);
        let _allTotal = res.data.data.total;
        let _goodTotal = res.data.data.goodCount;
        let _middleTotal = res.data.data.midCount;
        let _lowTotal = res.data.data.badCount;
        this.setData({
          badRate: _badRate,
          goodRate: _goodRate,
          midRate: _midRate,
          allTotal: _allTotal,
          goodTotal: _goodTotal,
          middleTotal: _middleTotal,
          lowTotal: _lowTotal,
          total: res.data.data.total,
          score: _score,
          score1: _score1 + ".0",
        })
        //重置评论数组内容
        this.resetFn(_allTotal, _goodTotal, _middleTotal, _lowTotal);
      }
    }).catch(err=>{
      console.warn(err);
    })
  },

  onReady: function() {
  },
  getLocationPromise() {
    return new Promise((resolve, reject) => {
      wx.getLocation({
        type: 'gcj02',
        success: (res) => {
          resolve(res)
        }
      })
    })
  },
  getInfoPromise(paramsId) { // 请求教练详情
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getTeacherInfo',
        data: {
          id: paramsId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (err) => {
          reject(err);
        }
      })
    })
  },
  getBranchSchoolInfo(branchId) { // 请求场地信息
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getSchoolAreaVo',
        data: {
          areaId: branchId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (err) => {
          reject(err);
        }
      })
    })
  },
  getIsAttention(optionsTid){ //获取是否关注
    return new Promise((resolve,reject)=>{
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getFansVo',
        data: {
          id: optionsTid,
          type: 'TEACHER',
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          console.log(res);
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: (res) => {

        }
      });
    })
  },
  getAttentionPromise(optionsTid) { //请求关注和非关注
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/attention',
        data: {
          id: optionsTid,
          type: 'TEACHER',
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          console.log(res);
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: (res) => {

        }
      });
    })
  },
  getDistance: function(lat1, lng1, lat2, lng2) { // 计算距离
    lat1 = lat1 || 0;
    lng1 = lng1 || 0;
    lat2 = lat2 || 0;
    lng2 = lng2 || 0;
    var rad1 = lat1 * Math.PI / 180.0;
    var rad2 = lat2 * Math.PI / 180.0;
    var a = rad1 - rad2;
    var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
    var r = 6378137;
    return (r * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad1) * Math.cos(rad2) * Math.pow(Math.sin(b / 2), 2)))).toFixed(0)
  },
  //点击tab切换
  swichNav(res) {
    console.log(res);
    if (this.data.currentType == res.detail.current) return;
    this.setData({
      currentType: res.detail.currentNum,
    })
    //全部
    if (res.detail.currentNum == 0){
      this.getEvaluatePromise(1, 10, this.data.teacherId, 'ALL').then(res => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          const _allArr = res.data.data.records;
          let _allArr1 = [];
          for (let i of _allArr) {
            let o = Object.assign({}, i);
            o.time = this.timestampToTime(i.createDate).split(' ')[0].split('-').join('.');
            _allArr1.push(o);
          }
          this.setData({
            allArr: _allArr1,
            allTotal: res.data.data.records.length,
          })
        }
      }).catch(err => {
        console.warn(err);
      });
    }
    //好
    if (res.detail.currentNum == 1){
      this.getEvaluatePromise(1, 10, this.data.teacherId, 'GOOD').then(res => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          const _allArr = res.data.data.records;
          let _allArr1 = [];
          for (let i of _allArr) {
            let o = Object.assign({}, i);
            o.time = this.timestampToTime(i.createDate).split(' ')[0].split('-').join('.');
            _allArr1.push(o);
          }
          this.setData({
            goodArr: _allArr1,
          })
        }
      }).catch(err => {
        console.warn(err);
      });
    }
    //中
    if (res.detail.currentNum == 2){
      this.getEvaluatePromise(1, 10, this.data.teacherId, 'MIDDLE').then(res => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          const _allArr = res.data.data.records;
          let _allArr1 = [];
          for (let i of _allArr) {
            let o = Object.assign({}, i);
            o.time = this.timestampToTime(i.createDate).split(' ')[0].split('-').join('.');
            _allArr1.push(o);
          }
          this.setData({
            middleArr: _allArr1,
          })
        }
      }).catch(err => {
        console.warn(err);
      });
    }
    //差
    if (res.detail.currentNum == 3){
      this.getEvaluatePromise(1, 10, this.data.teacherId, 'LOW').then(res => {
        console.log(res);
        if (res.data.code == 0 && res.data.message == "成功") {
          const _allArr = res.data.data.records;
          let _allArr1 = [];
          for (let i of _allArr) {
            let o = Object.assign({}, i);
            o.time = this.timestampToTime(i.createDate).split(' ')[0].split('-').join('.');
            _allArr1.push(o);
          }
          this.setData({
            lowArr: _allArr1,
          })
        }
      }).catch(err => {
        console.warn(err);
      });
    }
  },
  scrollFunc(e) {
    // console.log(e);
    if (e.detail.scrollTop > 360) {
      this.setData({
        scrollTp: 365
      })
    } else {
      this.setData({
        scrollTp: 0
      })
    }

    if (e.detail.scrollTop > 878) {
      this.setData({
        curNavId: 2,
      })
    } else {
      this.setData({
        curNavId: 1,
      })
    }
  },
  handleClass(e) {
    this.setData({
      currentTab: e.target.dataset.id,
      downPayment: e.target.dataset.pay,
      orderId: e.target.dataset.id,
    })

  },
  switchTab(e) {
    console.log(e);
    this.setData({
      curNavId: e.currentTarget.dataset.id,
    });
    if (e.currentTarget.dataset.id == 2){
      this.setData({
        scrollTop: 1000,
      })
    } else if (e.currentTarget.dataset.id == 1){
      this.setData({
        scrollTop: 355,
      })
    }
  },
  enregister: function(e) { // 支付功能
    // 已报名不能重复
    // console.log(this.data.buyInsurance);    
    let orderId = this.data.orderId;
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/attend',
      data: {
        classId: orderId, // 班级编号
        teacherId: this.data.teacherId,
        buyInsurance: this.data.buyInsurance,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        // console.log(res);
        if (res.data.code == 0 && res.data.message == '成功') {
          let orderNumber = res.data.data.orderNumber;
          this.setData({
            createOrderNumber:orderNumber,
            createOrderId:res.data.data.id,
          })
          this.payMoney(orderNumber); // 传入订单号
        }else{
          this.failModal(res.data);
        }
      },
      fail: (res)=> {
        this.failModal(res);
      },
      complete: function(res) {},
    })
  },
  payMoney: function(orderNum) {
    console.log(orderNum);
    wx.request({
      url: 'https://www.haitunxueche.com/pay/wxPay',
      data: {
        orderNum: orderNum,
      },
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if (res.data.code == 0) {
          const _dat = res.data.data;
          this.wxPay(_dat,orderNum);
        } else {
          this.failModal(res.data);
        }
      },
      fail: (res) => {
        this.failModal(res.data);
      },
      complete: function(res) {},
    })
  },
  wxPay: function (obj, orderNum) {
    // console.log(obj);
    if (!obj.timeStamp || !obj.nonceStr || !obj.packageStr || !obj.paySign || !obj.signType) {
      return;
    }
    wx.requestPayment({
      timeStamp: obj.timeStamp,
      nonceStr: obj.nonceStr,
      package: obj.packageStr,
      signType: obj.signType,
      paySign: obj.paySign,
      success: (res) =>{
        if (res.errMsg.indexOf('requestPayment:ok') >= 0) {
          wx.showLoading({
            title: '确认订单',
            mask: true,
          })
          let limitFlag;
          const timeLimit = setTimeout( ()=>{ // 12秒无返回状态进入我的订单
            limitFlag = true;
            wx.navigateTo({
              url: '../myOrder/myOrder',            
            })
          },12000);
          let loopRequestPayState = () =>{
            if(limitFlag){
              return;
            }
            wx.request({
              url: 'https://www.haitunxueche.com/pay/getPayStatus',
              data: {
                orderNum: orderNum,
              },
              header: {
                Authorization: getApp().globalData.login.token,
              },
              method: 'GET',      
              success: (res) =>{
                if(res.data.code===0){
                  if(res.data.data!=2){
                    return loopRequestPayState();
                  }else{
                    // 体检提示
                    wx.setStorageSync('notionBodyCheck', '支付完成后提示体检');
                    wx.hideLoading();
                    clearTimeout(timeLimit);
                    this.handleAfterBand();
                    // wx.reLaunch({
                    //   url: '../start/start'
                    // }); // 支付成功重启小程序
                    return;
                  }
                }else{
                  this.failModal(res);
                }
              },
              fail: (res) => {   
                this.failModal(res);
              },
              complete: function(res) {},
            })
          }  
          loopRequestPayState();// 支付成功后开始轮询订单状态
        }
      },
      fail: (res)=> {
        console.warn(res);
        this.failModal(res);
      },
      complete: function(res) {
        // console.log('complete',res);
      },
    })
  },
  handleAfterBand(){ // 支付成功后的操作
    wx.request({
      url: 'https://www.haitunxueche.com/student/index/getClasses',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: function(res) {
        if(res.data.code==0){
          const user = new userModule();
          user.reloadStudy(()=>{
            wx.switchTab({
              url: '../rdv/rdv',
            });
          })
        }else{
          wx.reLaunch({
            url: '../start/start'
          });
        }
      },
      fail: function(res) {
        wx.reLaunch({
          url: '../start/start'
        });
      },
      complete: function(res) {},
    })
  },
  failModal(d){
    wx.showModal({
      title: '购买失败',
      content: '',
      showCancel: false,     
      confirmText: '确定',
    })
    if (this.data.createOrderNumber){
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/closeOrder',
        data: {
          orderNumber: this.data.createOrderNumber,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        success: (res)=> {
          console.log('支付失败取消报名订单：' + this.data.createOrderNumber)
        },
        fail: function(res) {},
        complete: function(res) {},
      })
    }
    console.warn(d);
  },
  handlePicc(e) {
    this.setData({
      piccActive: !this.data.piccActive,
      buyInsurance: !this.data.buyInsurance,
    })
    console.log(this.data.buyInsurance);
  },
  handleAttention(e) {
    console.log(this.data.teacherId);
    this.getAttentionPromise(this.data.teacherId).then((res)=>{
      console.log("coming");
      if(res.data.code == 0 && res.data.message == "成功"){
        this.setData({
          attention: res.data.data.attention,
        })
      }
    }).catch(e=>{
      console.warn(e);
    });
  },
  handleTraining(e){
    console.info(e);
    let _lat1 = e.currentTarget.dataset.lat;
    let _lng1 = e.currentTarget.dataset.lng;
    this.getMap(_lat1, _lng1);
  },
  getPicsPromise(_tid) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getTeacherPicVo',
        data: {
          id: _tid,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }
      })
    })
  },
  getEvaluatePromise(_page, _pageSize, _teacherId, _type){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/pageEvaluateVo',
        data: {
          page: _page,
          pageSize: _pageSize,
          teacherId: _teacherId,
          type: _type,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () {}

      })
    })
  },
  //获取教练学员列表
  getStudentListPromise(_teacherId){
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/listTeacherStudentVo',
        data: {
          teacherId: _teacherId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }

      })
    })
  },
  handleDrivingSchool(e){
    let _tid = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../drivingSchool/drivingSchool?tid=' + _tid,
    })
  },
  toWeb(){
    const str = '1072173617632022528';
    const time = '2018年12月11日 00:57:29';
    wx.navigateTo({
      url: '../articleReading/articleReading?id=' + str + '&time=' + time,
    })
  },
  //获取平均评分
  getAverageScore(_teacherId){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getEvaluateStatisticsVo',
        data: {
          id: _teacherId,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }

      })
    })
  },
  //重置评论数组内容
  resetEvaluateArr(_allTotal, _goodTotal, _middleTotal, _lowTotal){
    let tabsList = [{
      "name": "全部",
      "count": _allTotal,
    }, {
      "name": "好评",
        "count": _goodTotal,
    }, {
      "name": "中评",
        "count": _middleTotal,
    }, {
      "name": "差评",
        "count": _lowTotal,
    }]

    return tabsList
  },
  resetFn(_allTotal, _goodTotal, _middleTotal, _lowTotal){
    this.setData({
      slideTabsData: this.resetEvaluateArr(_allTotal, _goodTotal, _middleTotal, _lowTotal),
    })
  },
  timestampToTime(timestamp) {
    // var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y + M + D + h + m + s;
  },
  getMap(_lat, _lng) {
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
        });
        var myAmap = new QQMapWX({ key: 'HTQBZ-FN3WI-IIRG7-5JWI4-74OFZ-AYFFJ' });
        // 调用接口
        myAmap.reverseGeocoder({
          location: {
            latitude: _lat,
            longitude: _lng,
          },
          success: (res) => {
            console.log(res);
            if (res.status == 0) {
              let _address = res.result.address;
              console.log(_address);
              wx.openLocation({
                latitude: _lat,
                longitude: _lng,
                name: _address,
                scale: 15,
              })
            }
          },
          fail: function (res) {
            console.log(res);
          },
          complete: function (res) {
            console.log(res);
          }
        });
      }
    })
  },
  toCourDetail(e){
    let pfPrice = e.currentTarget.dataset.price;
    wx.navigateTo({
      url: '../courDetail/courDetail?coachprice=' + pfPrice,
    })
  },
  toEnsurance(){
    wx.navigateTo({
      url: '../courDetail/courDetail?baoxian',
    })
  },
  cjwt(){
    wx.navigateTo({
      url: '../wenzhang/wenzhang?key=cjwt',
    })
  }
})