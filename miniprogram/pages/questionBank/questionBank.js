// pages/find/find.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modalCtrl: 'hidden',
    linePosition: 'left:0rpx',
    category: '0',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  testModal() {
    this.setData({
      modalCtrl: 'show'
    })
  },
  onCloseEvent() {
    this.setData({
      modalCtrl: 'hidden'
    })
  },
  testTabp() {
    console.log('test tap');
  },
  changeCategory(e) {
    // console.log(e)
    const t = e.currentTarget.dataset.cat;
    const diff = 187.5 * parseInt(t);
    console.log(diff);
    this.setData({
      category: t,
      linePosition: 'left:' + diff + 'rpx',

    })
  },
  warnNotion() {

    wx.showModal({
      title: '温馨提示',
      content: '题库更新中，暂时无法使用',
      confirmText: '确定',
      confirmColor: '',
      showCancel: false,
      success: function (res) {
        // wx.switchTab({
        //   url: '../index/index',
        // })
      },
    })

  }
})