// pages/showPic/showPic.js
Page({
  data: {
    wdHeight: 0,
    index: 1,
    lifePicsLength: 0,
    workPicsLength: 0,
    totalPicsLength: 0,
    lifePicsArr: [],
    workPicsArr: [],
    totalPicsArr: [],
  },
  onLoad: function (options) {
    var log = console.log.bind(console);
    log(options);
    let tid = options.tid;
    this.getPicsPromise(tid).then(res => {
      log(res);
      if (res.data.code == 0 && res.data.message == "成功") {
        let lifePicsLength = res.data.data.lifePics.length;
        let workPicsLength = res.data.data.workPics.length;
        let totalPicsLength = lifePicsLength + workPicsLength;
        let lifePicsArr = res.data.data.lifePics;
        let workPicsArr = res.data.data.workPics;
        let totalPicsArr = [...new Set([options.it,...lifePicsArr, ...workPicsArr])];
        log(totalPicsArr);
        log(lifePicsLength, workPicsLength, totalPicsLength);
        this.setData({
          totalPicsLength: totalPicsLength,
          lifePicsLength: lifePicsLength,
          workPicsLength: workPicsLength,
          totalPicsArr: totalPicsArr,
          lifePicsArr: lifePicsArr,
          workPicsArr: workPicsArr,
        })
      } else {
        console.error(res);
      }
    }).catch(reason => {
      console.log(reason);
    })
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          wdHeight: res.windowHeight
        })
      },
    })

  },
  onReady: function(){

  },
  handlePic(e){
    console.log(e);
    this.setData({
      index: e.detail.current + 1,
    })
  },
  getPicsPromise(_tid) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: 'https://www.haitunxueche.com/student/index/getTeacherPicVo',
        data: {
          id: _tid,
        },
        header: {
          Authorization: getApp().globalData.login.token,
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          resolve(res);
        },
        fail: (error) => {
          reject(error);
        },
        complete: function () { }
      })
    })
  }
})