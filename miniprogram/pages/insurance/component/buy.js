// pages/insurance/component/buy.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    phoneValidNumber: String,
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toList(){
      wx.navigateTo({
        url: '/pages/insurance/myInsurance',
      })
    },
    buyInsurrance(){
      this.triggerEvent('buyInsurrance',{},{});
    }
  }
})
