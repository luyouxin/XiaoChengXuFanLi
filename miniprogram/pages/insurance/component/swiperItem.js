// pages/insurance/componen/swiperItem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    users:[
      {
        name:'W先生',
        work:'公司职员',
        img:'/assert/img/mrW.png',
        say:'身边好几位同事考试太紧张，都没考好。还好买了驾考险，补考的费用由保险公司赔付，省了近千元啊，否则那点工资全都得搭进去。',
      },
      {
        name:'Z先生',
        work:'高校学生',
        img:'/assert/img/mrZ.png',
        say:'不敢保证每科都一次过，考试费那么贵，还是买个保险安心点，反正也两百多，相对其他驾考保险便宜多了。',
      },
      {
        name:'P小姐',
        work:'银行职员',
        img:'/assert/img/missP.png',
        say:'对于考试容易紧张的女司机来说，这个保险太实在了。不用担心无底洞的补考费，统统都由保险公司赔付，很实惠，很值得推荐！',
      },
    ],
    leftDistance:'0rpx',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeSw(e){
      console.log(e);
      const index = e.detail.current;
      this.setData({
        leftDistance: (index * 90 ) + 'rpx',
      })
    }
  }
})
