// pages/insurance/myInsurance.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    insuranceList:[
      {
        name:'驾考无优险',
        beneficer:'何小杰',
        price:'250.00',
        timeRange:'2019-03-10~2019-09-10'
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },
  loadData(){
    wx.request({
      url: getApp().globalData.serverAddress + 'student/listInsure',
      data: '',
      header: {
        Authorization: getApp().globalData.login.token,
      },
      method: 'GET',
      success: (res) => {
        if(res.data.code === 0){
          console.log(res.data.data);
          this.setData({
            insuranceList: res.data.data,
          })
        }
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  }

})